import { Component, OnInit } from '@angular/core';
import { CustomServiceService } from '../../ez-services/custom-service.service';



@Component({
  selector: 'app-chartsdemo',
  templateUrl: './chartsdemo.component.html',
  styleUrls: ['./chartsdemo.component.css']
})
export class ChartsdemoComponent implements OnInit {

  data: any = {};

  width = 600;
  height = 300;
  type: any = '';
  dataFormat = 'json';
  dataSource: any;
  Piechartdata: any;
  Barchartdata:any;
 


  constructor(private service: CustomServiceService) { }

  ngOnInit() {
    this.PieChart();
    this.Barchart();
  }


  PieChart() {
    this.type = 'pie3d';
    
    let obj: any = {};
    this.service.PostMethodWithHeaders(obj, "api/AgentDeliverySummary").subscribe(response => {
      this.Piechartdata = {
        "chart": {
          
          "caption": "Delivery Summary",
          // "subcaption": "For a net-worth of $1M",
          "captionFont": "calibri",
          "captionFontSize": "20",
          "captionFontColor": "#993300",
          "captionFontBold": "1",
          "useRoundEdges": "1",

          "palettecolors": "#FFA500, #28c3be,#008001", /*colors for graph  */

          "baseFont": "calibri",
          "baseFontSize": "15",
          "baseFontColor": "#0066cc",

          "showvalues": "1",
          "showpercentintooltip": "1", /*Percentage on tooltip*/
          // "numberprefix": "$",
          "enablemultislicing": "0",  /*auto close blocks when click on one block, if not '1'*/
          " useDataPlotColorForLabels": "1",
          // "plotHighlightEffect": "fadeout|anchorBgColor=ff0000, anchorBgAlpha=50",
          "showLegend": "1", /*To show labels on footer*/
          "interactiveLegend": "1", /*To Expand each label block when clicked the label , if not use '0' */
          "theme": "gammel",  /*Fusion or Gammel or Candy or Zune or Ocean*/

          "legendPosition": "bottom", /*Bottom or right for label position*/
          "legendAllowDrag": "1", /*To drag the legends one place to another , if not '0'*/
          "legendIconScale": "1.5", /*size for legend i.e. 1,2,3 .....*/
          "legendItemFontBold": "1",
          "legendItemFont": "calibri",
          "legendItemFontSize": "12",
          "legendItemFontColor": "#30328a",
          // "reverseLegend": "1" /*To change the order of legends*/


          "canvasBorderThickness": "1",
          "showAlternateHGridColor": "0",
          "bgColor": "#eeeeee",
         
          // "canvasbgAlpha": "100",
          // "canvasBgRatio": "40,60",
          // "canvasBgAngle": "0"

        },
        "data": [
          {
            "label": "Today",
            "value": response[0].TodayDeliveries
          },
          {
            "label": "Weekly",
            "value": response[0].WeekDeliveries
          },
          {
            "label": "Monthly",
            "value": response[0].MonthDeliveries
          }
        ]
      }


    }, err => {

    })

  }

  Barchart() {

    this.service.getwithId("api/AreaWiseBooking", "hyderabad").subscribe(response => {
      console.log(response);

      var bardata: any = [];
      for (var i = 0; i < response.length; i++) {

        bardata.push({
          'label': response[i].AreaName, 'value': response[i].TotalBookings
        });

      }





     this. Barchartdata = {
        "chart": {
          "caption": "Income and Payments",
          // "subcaption": "For a net-worth of $1M",
          "captionFont": "calibri",
          "captionFontSize": "20",
          "captionFontColor": "#993300",
          "captionFontBold": "1",
          "useRoundEdges": "1",

          "palettecolors": "#FFA500, #28c3be,#008001", /*colors for graph  */

          "baseFont": "calibri",
          "baseFontSize": "15",
          "baseFontColor": "#0066cc",

          "showvalues": "1",
          "showpercentintooltip": "1", /*Percentage on tooltip*/
          // "numberprefix": "$",
          "enablemultislicing": "0",  /*auto close blocks when click on one block, if not '1'*/
          " useDataPlotColorForLabels": "1",
          // "plotHighlightEffect": "fadeout|anchorBgColor=ff0000, anchorBgAlpha=50",
          "showLegend": "1", /*To show labels on footer*/
          "interactiveLegend": "1", /*To Expand each label block when clicked the label , if not use '0' */
          "theme": "gammel",  /*Fusion or Gammel or Candy or Zune or Ocean*/

          "legendPosition": "bottom", /*Bottom or right for label position*/
          "legendAllowDrag": "1", /*To drag the legends one place to another , if not '0'*/
          "legendIconScale": "1.5", /*size for legend i.e. 1,2,3 .....*/
          "legendItemFontBold": "1",
          "legendItemFont": "calibri",
          "legendItemFontSize": "12",
          "legendItemFontColor": "#30328a",
          // "reverseLegend": "1" /*To change the order of legends*/


          "canvasBorderThickness": "1",
          "showAlternateHGridColor": "0",
          "bgColor": "#eeeeee",
          // "canvasbgAlpha": "100",
          // "canvasBgRatio": "40,60",
          // "canvasBgAngle": "0"



        },
        "data": bardata
      }

    //  console.log(this.dataSource);


    }, err => {

    })


  }

  


}
