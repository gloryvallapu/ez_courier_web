import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule , ReactiveFormsModule,Validators} from '@angular/forms';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserbookingComponent } from './components/userbooking/userbooking.component';
import { AgentbookingComponent } from './components/agentbooking/agentbooking.component';
import { CustomServiceService } from './ez-services/custom-service.service';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FooterComponent } from './components/footer/footer.component';
import { AuthGuard } from './auth/auth.guard';
import { from } from 'rxjs/observable/from';
import { NumberDirective } from './number.directive';
import {MatDialogModule} from '@angular/material/dialog';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import {ItemMasterComponent} from './components/item-master/item-master.component'
import { RateMappingComponent } from './components/rate-mapping/rate-mapping.component';
import { CategoryMasterComponent } from './Components/category-master/category-master.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {NgxPaginationModule} from 'ngx-pagination';
import { AdminDashboardComponent } from './components/admin-dashboard/admin-dashboard.component';
import { AgentCreationComponent } from './components/agent-creation/agent-creation.component';
import { UserCrationComponent } from './components/user-cration/user-cration.component';
import { HomeComponent } from './components/home/home.component';
// import { LocationComponent } from './location/location.component';

import { AgentWiseReportComponent } from './components/agent-wise-report/agent-wise-report.component';
import { AgentPaymentsComponent } from './components/agent-payments/agent-payments.component';
import { PaymentRegisterComponent } from './components/payment-register/payment-register.component';
import { UserbookingRegisterComponent } from './Components/userbooking-register/userbooking-register.component';

// Import angular-fusioncharts
import { FusionChartsModule } from 'angular-fusioncharts';

// Import FusionCharts library
import * as FusionCharts from 'fusioncharts';

// Load FusionCharts Individual Charts
import * as Charts from 'fusioncharts/fusioncharts.charts';

import { CareersComponent } from './components/careers/careers.component';
import { AgentRequestsComponent } from './components/agent-requests/agent-requests.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { RefundOrdersComponent } from './components/refund-orders/refund-orders.component';



// Use fcRoot function to inject FusionCharts library, and the modules you want to use
FusionChartsModule.fcRoot(FusionCharts, Charts)

@NgModule({
  declarations: [
    AppComponent,
    UserbookingComponent,
    AgentbookingComponent,
    NavbarComponent,
    FooterComponent,
    NumberDirective,
    LoginComponent,
    RegisterComponent,
    ItemMasterComponent,
    RateMappingComponent,
    CategoryMasterComponent,
    AdminDashboardComponent,
    AgentCreationComponent,
    UserCrationComponent,
    HomeComponent,
    AgentWiseReportComponent,
    AgentPaymentsComponent,
    PaymentRegisterComponent,
    UserbookingRegisterComponent,
    CareersComponent,
    AgentRequestsComponent,
    ForgotPasswordComponent,
    ChangePasswordComponent,
    RefundOrdersComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,HttpModule, 
    ReactiveFormsModule,
    GooglePlaceModule,
    FormsModule,
    MatDialogModule,
    BrowserAnimationsModule,
    AngularFontAwesomeModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    FusionChartsModule


    // DatepickerModule

    // MatDatepickerModule,
    // MatNativeDateModule 
   
  ],
  providers: [CustomServiceService, AuthGuard],
  entryComponents:[LoginComponent,RegisterComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
