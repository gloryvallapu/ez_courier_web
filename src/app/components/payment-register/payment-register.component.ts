import { Component, OnInit } from '@angular/core';
import { CustomServiceService } from '../../ez-services/custom-service.service';
import { HttpErrorResponse } from "@angular/common/http";
import * as $ from 'jquery';

@Component({
  selector: 'app-payment-register',
  templateUrl: './payment-register.component.html',
  styleUrls: ['./payment-register.component.css']
})
export class PaymentRegisterComponent implements OnInit {
  dtAgents: any;
  PaymentDetails: any = {};
  ctselectedValue: any = 0;
  dtpaymentDetails: any = {};
  txtpaymentsearch: any;
  txtnet: any;
  objtotal: any = {};
  iagentID: any;
  txtpaidnet: any;

  lblrecordCount: any = 0;
  lblAgentName: any = "";
  dtViewPaymentdetails: any = {};
  txtviewpaymentsearch: any;
  pageNo: any;

  lblTotAmuntCollected: any = 0;
  lblTotAmountpaid: any = 0;


  constructor(public service: CustomServiceService) { }

  ngOnInit() {
    this.PaymentDetails.txtFromDate = new Date().toISOString().split('T')[0];
    this.PaymentDetails.txtToDate = new Date().toISOString().split('T')[0];

  }

  // Getting all the Agents
  // GetAgentDetails() {


  //   let Url = "api/Agent";
  //   this.service.getmethod(Url).subscribe(response => {

  //     this.dtAgents = response.filter(Agent => Agent.IsActive === true);
  //     console.log(this.dtAgents);

  //   }, err => {

  //   })

  // }




  // To view Payment Details
  ViewPaymentDetails() {

    if (!this.PaymentDetails.txtFromDate) {
      alert("Please Select From Date");
      $("#txtFromDate").focus();

    }
    else if (!this.PaymentDetails.txtToDate) {
      alert("Please Select To Date");
      $("#txtToDate").focus();

    }
    else if (this.PaymentDetails.txtToDate < this.PaymentDetails.txtFromDate) {
      alert("To Date Should be Greather than From Date");
      $("#txtToDate").focus();
    }
    else {
      this.lblTotAmuntCollected = 0;
      this.lblTotAmountpaid = 0;

      let ViewPaymentDetails: any = {};


      ViewPaymentDetails.FromDate = this.PaymentDetails.txtFromDate;
      ViewPaymentDetails.ToDate = this.PaymentDetails.txtToDate;


      this.service.PostMethodWithHeaders(ViewPaymentDetails, 'api/PaymentRegister').subscribe(res => {

        if (res.length > 0) {
          this.dtpaymentDetails = res;
          this.lblrecordCount = this.dtpaymentDetails.length;
          var amountcollected: any = 0;
          var amountpaid: any = 0;

          for (var i = 0; i < this.dtpaymentDetails.length; i++) {
            amountcollected = amountcollected + this.dtpaymentDetails[i].AmountCollected;
            amountpaid = amountpaid + this.dtpaymentDetails[i].AgentAmount;

          }


          this.objtotal.txtnet = amountcollected
          this.objtotal.txtpaidnet = amountpaid

          this.dtpaymentDetails.forEach(element => {
            this.lblTotAmuntCollected += element.AmountCollected;
            this.lblTotAmountpaid += element.AgentAmount;

          });

        }
        else {
          this.dtpaymentDetails = {};
          alert("No Data Available")
        }


      }, (err: HttpErrorResponse) => {

        if (err.error instanceof Error) {
          console.log("client side error");
        }
        else {
          console.log("Server side error");
        }

      })
    }

  }



  // To view Agent wise PaymentDetails

  ViewAgentWisePaymentDetails(row) {
    

    this.service.getwithId('api/AgentWisePaymentSummary', row.AgentID).subscribe(res => {

      if (res.length > 0) {
        this.lblAgentName = row.AgentName;
        this.dtViewPaymentdetails = res;
        this.lblrecordCount = this.dtViewPaymentdetails.length;


      }
      else {
        this.dtViewPaymentdetails = {};
        alert("No Data Available")
      }


    }, (err: HttpErrorResponse) => {

      if (err.error instanceof Error) {
        console.log("client side error");
      }
      else {
        console.log("Server side error");
      }

    })


  }


}
