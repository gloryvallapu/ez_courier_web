import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { CustomServiceService } from '../../ez-services/custom-service.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  isLoggedIn$: Observable<boolean>;
  
  constructor(private Service: CustomServiceService) { }

  ngOnInit() {
    this.isLoggedIn$ = this.Service.isLoggedIn; // {2}
  }

}
