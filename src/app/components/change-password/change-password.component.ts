import { Component, OnInit } from '@angular/core';
import { CustomServiceService } from '../../ez-services/custom-service.service';
import * as $ from 'jquery';
import * as crypto from "crypto-js";
@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  constructor(public service: CustomServiceService) { }

  txtNewPwd: any;
  txtCnfPwd: any;
  UserDetails: any;

  ngOnInit() {
  }

  fnChangePassword(): void {
    let roy = this;
    if (!roy.txtNewPwd) {
      alert("Please enter New Password");
      $("#txtNewPwd").focus();
    }
    else if (!roy.txtCnfPwd) {
      alert("Please enter Confirm Password");
      $("#txtCnfPwd").focus();
    }
    else if (roy.txtNewPwd !== roy.txtCnfPwd) {
      alert("Password and Confirm Password mismatch");
      $("#txtNewPwd").focus();
    }
    else {

      roy.UserDetails = JSON.parse(localStorage.getItem("UserDetails"));
      //console.log(roy.UserDetails.UserId);
      //console.log(roy.txtNewPwd);

      let encpwd = crypto.DES.encrypt(roy.txtNewPwd, roy.service.key).toString();

      let objParams: any = {};
      objParams.Password = encpwd;
      objParams.UserID = roy.UserDetails.UserId;

      //console.log(objParams);

      // objParams
      roy.service.PostMethodWithHeaders(objParams, "api/ChangePassword").subscribe(response => {
        roy.txtNewPwd = null;
        roy.txtCnfPwd = null;
        $("#txtNewPwd").focus();
        alert("Password changed successfully");
      }, err => {

      })
    }
  }

}
