import { Component, OnInit } from '@angular/core';
import { Headers, RequestOptions, Http } from '@angular/http';
import { CustomServiceService } from '../../ez-services/custom-service.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-careers',
  templateUrl: './careers.component.html',
  styleUrls: ['./careers.component.css']
})
export class CareersComponent implements OnInit {

  objCareer: any = {};

  constructor(public service: CustomServiceService) {

  }

  ngOnInit() {

  }

  fnRequest(objParams): void {
    let roy = this;

    if (!objParams.FullName) {
      alert("please enter full name");
       $("#FullName").focus();
    }
    else if (!objParams.MobileNo) {
      alert("please enter mobile number");
       $("#MobileNo").focus();
    }
    else if (!objParams.EmailID) {
      alert("please enter email id");
       $("#EmailID").focus();
    }
    else if (!roy.service.fnValidateEmailAddress(objParams.EmailID)) {
      alert("please enter valid email id");
       $("#EmailID").focus();
    }
    else {
      //console.log(objParams);

      roy.service.PostMethodWithHeaders(objParams, 'api/Careers').subscribe(royRes => {
        //console.log(JSON.stringify(royRes));
        if (royRes[0].StatusCode.toString() == "444") {
          
          alert("Request submitted");

        }
        else {
          alert(royRes[0].Status);
        }
        roy.Clear();
      }, err => {

      })

    }
  }

  Clear(): void {
    let roy = this;
    roy.objCareer = {};
  }

}
