import { Component, OnInit } from '@angular/core';
import { CustomServiceService } from '../../ez-services/custom-service.service';
import { HttpErrorResponse } from "@angular/common/http";
// import $ from 'jquery';
// import 'bootstrap';
declare var $: any;




@Component({
  selector: 'app-agent-payments',
  templateUrl: './agent-payments.component.html',
  styleUrls: ['./agent-payments.component.css']
})
export class AgentPaymentsComponent implements OnInit {

  dtAgents: any;
  AgentDetails: any = {};

  AgentregDetails: any = {};
  ctselectedValue: any = 0;
  dtbookingDetails: any = {};
  txtAgentregSearch: any;
  lblBookingID: any = "";
  lblAgentAmount: any = 0;
  lblAgentName: any = "";
  lblAgentID: any = "";
  txtPaymentType: any = "";
  txtDesc: any = "";
  // lblTotalCount: any = 0
  lblBonus: any;
  lblTotal: any;
  pageNo: any;

  lblTotFare: any = 0;
  lblTotCommission: any = 0;

  lblTotBonus: any = 0;
  lblTotdistance: any = 0;



  constructor(public service: CustomServiceService) { }





  ngOnInit() {
    this.GetAgentDetails();
    this.AgentDetails.txtFromDate = new Date().toISOString().split('T')[0];
    this.AgentDetails.txtToDate = new Date().toISOString().split('T')[0];
  }

  // Getting all the Agents
  GetAgentDetails() {

    let Url = "api/Agent";
    this.service.getmethod(Url).subscribe(response => {

      this.dtAgents = response.filter(Agent => Agent.IsActive === true);

    }, err => {

    })

  }


  GetPendingPaymentDetails() {
    if (!this.AgentDetails.txtFromDate) {
      alert("Please Select From Date");
      $("#txtFromDate").focus();

    }
    else if (!this.AgentDetails.txtToDate) {
      alert("Please Select To Date");
      $("#txtToDate").focus();
    }
    else if (this.AgentDetails.txtToDate < this.AgentDetails.txtFromDate) {
      alert("To Date Should be Greather than From Date");
      $("#txtToDate").focus();
    }
    else {
      this.lblTotFare = 0;
      this.lblTotCommission = 0;

      this.lblTotBonus = 0;
      this.lblTotdistance = 0;

      let ViewAgentDetails: any = {};

      ViewAgentDetails.agentID = this.ctselectedValue;
      ViewAgentDetails.FromDate = this.AgentDetails.txtFromDate;
      ViewAgentDetails.ToDate = this.AgentDetails.txtToDate;

      this.service.PostMethodWithHeaders(ViewAgentDetails, 'api/AgentWisePayments').subscribe(res => {
        // console.log(JSON.stringify(res));
        if (res.length > 0) {
          this.dtbookingDetails = res.filter(ROY => ROY.IsPaidToAgent === false);
          console.log(JSON.stringify(this.dtbookingDetails))
          if (this.dtbookingDetails.length == 0) {
            alert("No Pending Payments");
          }
          // this.lblTotalCount = this.dtbookingDetails.length;

          this.dtbookingDetails.forEach(element => {
            this.lblTotFare += element.TotalFare;
            this.lblTotCommission += element.AgentComission;
            this.lblTotBonus += element.AgentBonus;
            this.lblTotdistance += element.Distance_KM;

          });

        }
        else {
          this.dtbookingDetails = {};
          alert("No Pending Payments");
        }
      }, (err: HttpErrorResponse) => {

        if (err.error instanceof Error) {
          console.log("client side error");
        }
        else {
          console.log("Server side error");
        }

      })
    }

  }


  MakePayment(row): void {
    //console.log(JSON.stringify(row));

    this.lblAgentName = row.AgentName;
    this.lblBookingID = row.BookingID;
    this.lblAgentAmount = row.AgentComission;
    this.lblAgentID = row.AgentID;
    this.lblBonus = row.AgentBonus;
    this.lblTotal = parseFloat(row.AgentComission) + parseFloat(row.AgentBonus);
    this.txtPaymentType = "";

    this.txtDesc = "";

  }

  PaymentDone(): void {


    if (!this.txtPaymentType) {
      alert("Please enter Payment type");
      $("#txtPaymentType").focus();
    }
    else {



      let ObjParams: any = {};
      ObjParams.agentID = this.lblAgentID;
      ObjParams.BookingId = this.lblBookingID;
      ObjParams.PaidAmount = this.lblTotal; //this.lblAgentAmount;
      ObjParams.Paidthrough = this.txtPaymentType;
      ObjParams.Bonus = this.lblBonus;
      ObjParams.Description = this.txtDesc;

      this.service.PostMethodWithHeaders(ObjParams, "api/AgentPaymentLogInsert").subscribe(res => {
        this.GetPendingPaymentDetails();
        alert("Payment Done");
        $("#PaymentModal").modal("hide");

      }, (err: HttpErrorResponse) => {

        if (err.error instanceof Error) {
          console.log("client side error");
        }
        else {
          console.log("Server side error");
        }

      })

    }
  }

}
