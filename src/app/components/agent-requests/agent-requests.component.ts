import { Component, OnInit } from '@angular/core';
import { CustomServiceService } from '../../ez-services/custom-service.service';

@Component({
  selector: 'app-agent-requests',
  templateUrl: './agent-requests.component.html',
  styleUrls: ['./agent-requests.component.css']
})
export class AgentRequestsComponent implements OnInit {


  dtRequests: any;
  pageNo:any;
  txtSearch:any;
  

  constructor(public service: CustomServiceService) { }

  ngOnInit() {
    let roy = this;
    roy.fnGetAgentRequestDetails();
  }

  fnGetAgentRequestDetails(): void {
    let roy = this;

    roy.service.getmethod("api/Careers").subscribe(royRes => {
      roy.dtRequests = royRes;
      console.log(royRes);
    }, err => {

    })
  }

}
