import { Component, OnInit, ViewChild, HostListener, NgZone } from '@angular/core';
import { CustomServiceService } from '../../ez-services/custom-service.service';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { AngularFontAwesomeComponent } from 'angular-font-awesome';
// import { } from '@types/googlemaps';
// import * as $ from 'jquery';
declare var $: any;

@Component({
  selector: 'app-userbooking',
  templateUrl: './userbooking.component.html',
  styleUrls: ['./userbooking.component.css'],

})


export class UserbookingComponent implements OnInit {

  @ViewChild('gmap') gmapElement: any;
  navIsFixed: boolean;
  UserDetails: any;
  MapsGlobelObject: any = [];
  objItems: any = {};
  map: google.maps.Map;
  // bookIds: any;
  register: boolean = false;
  ez_r_open: boolean = true;

  customerID: any = 0;

  ddlQty: any = 0;
  ddlItem: any = 0;
  ddlWeight: any = 0;

  dtItems: any = [];
  dtSelectedItems: any = [];
  dtQty: any = [{ Qty: 1 }, { Qty: 2 }, { Qty: 3 }, { Qty: 4 }, { Qty: 5 }];
  dtWeight: any = [{ Weight_Qty: '<= 1', Weight: 1 }, { Weight_Qty: '1 - 2', Weight: 2 }, { Weight_Qty: '2 - 3', Weight: 3 }, { Weight_Qty: '3 - 4', Weight: 4 }, { Weight_Qty: '4 - 5', Weight: 5 }];
  dtRates: any = [];


  lblETA: any = 0;
  lblFare: any = 0;
  lblDistance: any = 0;
  AmountforKM: any = 0;
  lblTotCancelCharges: any = 0;
  lblPromoOffer: any = 0;
  lblTotalAmount: any = 0;
  lblAppliedPromoCode: any = "";
  ShowbtnApply: boolean = true;
  InvoiceShow: boolean = false;
  divPayUSubmit: boolean = false;
  divFreeRideSubmit: boolean = false;
  txtPromoCode: any = '';
  dtPromoCodes: any = [];
  dtCancelCharges: any = [];
  RefIDwithCommas: string = '';
  totCancelCharges: any = 0;
  isPayU: boolean = false;
  IsPaymentDone: boolean = false;
  PaymentType: any; /* 0 ==> Free Ride, 1 ==> COD, 2 ==> Online Payment (PayU)  */



  //public searchElementRef: ElementRef;
  constructor(public service: CustomServiceService, private ngzone: NgZone) { }

  // Map Zoom
  @HostListener("window:scroll", [])
  onWindowScroll() {
    if (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop > 100) {
      this.navIsFixed = true;
    } else if (this.navIsFixed && window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop < 10) { this.navIsFixed = false; }
  } scrollToTop() {
    (function smoothscroll() {
      var currentScroll = document.documentElement.scrollTop || document.body.scrollTop; if (currentScroll > 0) {
        window.requestAnimationFrame(smoothscroll);
        window.scrollTo(0, currentScroll - (currentScroll / 5));
      }
    })();
  }


  // Page intiation
  ngOnInit() {

    this.loadMap();
    this.fnGetMultipleRequests();

    // this.getallBookItems();
    // this.getAllRates();
  }

  // get multiple Requests using fork join
  fnGetMultipleRequests(): void {

    let Roy = this;
    Roy.UserDetails = JSON.parse(localStorage.getItem("UserDetails"));
    Roy.customerID = Roy.UserDetails.UserId;
    // console.log(Roy.customerID);
    let objURL: any = {};
    objURL.urlBookItems = "api/BookingService";
    objURL.urlRates = "api/RateMapping";
    objURL.urlCancelCharges = "api/CancellationCharges/";

    let objParams: any = {};
    objParams.CustomerID = Roy.customerID;

    this.service.requestDataFromMultipleSources(objURL, objParams).subscribe(responseList => {

      this.dtItems = responseList[0].filter(ROY => ROY.IsActive === true);
      this.dtRates = responseList[1];
      this.dtCancelCharges = responseList[2];


      this.fnCalculateCancellationCharges();
      //console.log("Cancel Charges :" + JSON.stringify(this.dtCancelCharges));

    });

  }

  /* Calculate cancellation charges and concatinate refIDs for clear cancellation charges if payment done. */
  fnCalculateCancellationCharges(): any {
    let roy = this;

    roy.dtCancelCharges.forEach(element => {

      roy.totCancelCharges += element.CancellationCharges;
      roy.RefIDwithCommas += element.RefID + ',';

    });
    roy.lblTotCancelCharges = Math.round(roy.totCancelCharges);
    // console.log(roy.totCancelCharges + '  sgdsgsdgsgd  ' + roy.RefIDwithCommas)
  }




  // // Getting all the items
  // getallBookItems() {

  //   let Url = "api/BookingService";
  //   this.service.getmethod(Url).subscribe(response => {
  //     this.dtItems = response.filter(ROY => ROY.IsActive === true);
  //     // console.log(response);
  //   }, err => {

  //   })

  // }


  // getAllRates() {

  //   this.service.getmethod("api/RateMapping").subscribe(response => {
  //     this.dtRates = response;
  //     //console.log(JSON.stringify(this.dtRates));
  //   }, err => {

  //   })
  // }

  ezbookingToggle() {
    this.ez_r_open = !this.ez_r_open;
  }

  reg(reg) {
    if (reg === 1) {
      this.register = false;
    } else if (reg === 2) {
      this.register = true;
    }
  }


  loadMap() {
    //console.log(this.MapsGlobelObject)
    navigator.geolocation.getCurrentPosition((position) => {
      var myLatlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      var mapProp = {
        center: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
        zoom: 18,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
      };
      var marker = new google.maps.Marker({
        position: myLatlng,
        draggable: true,
        title: "Hello World!"
      });
      this.map = new google.maps.Map(this.gmapElement.nativeElement, { //this.gmapElement.nativeElement is map div id
        zoom: 14,
        center: { lat: position.coords.latitude, lng: position.coords.longitude }
      });
      //marker.setMap(this.map);
    });
  }

  handleAddressChange(address: any, arg, obj, map) {  //, callback, map, callback1

    //console.log("Address " + JSON.stringify(address));
    // console.log("Address " + JSON.stringify(address.address_components));
    // console.log("Reverse Address " + JSON.stringify(address.address_components.reverse()));
    // console.log("Format address "+ address.formatted_address);

    //callback ==> DisplayRoute() ,,,  callback1 ==> computeTotalDistanceTime() 

    let ROY = this;  /* if we use call method with 'this' keyword is not working */
    if (arg === 1) { // Source 

      // Initialize the Geocoder
      obj.address1 = address.name;
      var geocoder = new google.maps.Geocoder();
      if (geocoder) {
        geocoder.geocode({
          'address': address.name
        }, function (results, status) {
          if (status == google.maps.GeocoderStatus.OK) {

            ROY.MapsGlobelObject.Source = address.formatted_address; // Source Address
            ROY.MapsGlobelObject.FromLat = results[0].geometry.location.lat(); // From Latitude
            ROY.MapsGlobelObject.FromLong = results[0].geometry.location.lng(); // From Longitude


            var result = results[0];
            obj.originlat = result.geometry.location.lat();
            obj.originlng = results[0].geometry.location.lng();

            if (obj.originlat != undefined && obj.originlat != undefined && obj.destinationlat != undefined && obj.destinationlat != undefined) {
              var directionsDisplay = new google.maps.DirectionsRenderer({
                draggable: true,
                map: map
              });
              directionsDisplay.addListener('directions_changed', function () {
                // callback1(directionsDisplay.getDirections(), obj);
                ROY.computeTotalDistanceTime(directionsDisplay.getDirections(), obj);
              });
              var directionsService = new google.maps.DirectionsService;
              //callback(directionsService, directionsDisplay, obj, map);
              ROY.DisplayRoute(directionsService, directionsDisplay, obj, map);

              // Check Near by agents
              ROY.fnGetNearByAgents();
            }
          }
        });
      }
    } else if (arg === 2) { // Destination

      obj.address2 = address.name;
      var geocoder = new google.maps.Geocoder();
      if (geocoder) {
        geocoder.geocode({
          'address': address.name
        }, function (results, status) {
          if (status == google.maps.GeocoderStatus.OK) {

            ROY.MapsGlobelObject.Destination = address.formatted_address; // Destination Address
            ROY.MapsGlobelObject.ToLat = results[0].geometry.location.lat(); // To Latitude
            ROY.MapsGlobelObject.ToLong = results[0].geometry.location.lng(); // To Longitude

            obj.destinationlat = results[0].geometry.location.lat();
            obj.destinationlng = results[0].geometry.location.lng();
            if (obj.originlat != undefined && obj.originlat != undefined && obj.destinationlat != undefined && obj.destinationlat != undefined) {
              var directionsDisplay = new google.maps.DirectionsRenderer({
                draggable: true,
                map: map
              });
              directionsDisplay.addListener('directions_changed', function () {
                // callback1(directionsDisplay.getDirections(), obj);
                ROY.computeTotalDistanceTime(directionsDisplay.getDirections(), obj);
              });
              var directionsService = new google.maps.DirectionsService;
              // direction on map
              ROY.DisplayRoute(directionsService, directionsDisplay, obj, map);

              // Check Near by agents
              ROY.fnGetNearByAgents();
            }
          }
        });
      }

    }
  }

  /* Calculate distance and time */
  computeTotalDistanceTime(result, obj) {


    var distance = 0;
    var time: any = 0;
    var myroute = result.routes[0];
    for (var i = 0; i < myroute.legs.length; i++) {
      distance += myroute.legs[i].distance.value;
      time += myroute.legs[i].duration.value;
    }
    //console.log("time", time);

    this.lblETA = this.GetETA(time);
    this.lblDistance = distance / 1000;

    // calculating Amount for Distance
    if (this.dtRates.length > 0) {
      let MinKM: any = this.dtRates[0].KM;
      let KMAmount: any = this.dtRates[0].KMAmount;

      // get amount based on distance
      if (parseFloat(this.lblDistance) <= MinKM) {
        this.AmountforKM = KMAmount;
      }
      else if (parseFloat(this.lblDistance) > MinKM) {
        this.AmountforKM = (parseFloat(this.lblDistance) / MinKM) * KMAmount;
      }

      // if already applied promocode and again change the source/destination.
      this.lblPromoOffer = 0;
      this.txtPromoCode = null;
      this.ShowbtnApply = true;
      $("#txtPromoCode").prop('disabled', false);

      // values are not changed immediatly, for immediatly change we use ngzone
      this.ngzone.run(() => {
        this.lblFare = Math.round(this.AmountforKM);
        this.lblTotalAmount = this.lblFare + this.lblTotCancelCharges + this.lblPromoOffer;
      })
    }
  }


  /* Get Near by agents */
  fnGetNearByAgents() {
    let Roy = this;
    let NearByAgentsReqObj: any = {};
    NearByAgentsReqObj.userLat = Roy.MapsGlobelObject.FromLat;
    NearByAgentsReqObj.userLong = Roy.MapsGlobelObject.FromLong;
    NearByAgentsReqObj.distRadius = 10; // With in 10 km radius

    Roy.service.PostMethodWithHeaders(NearByAgentsReqObj, "api/NearByAgents").subscribe(RoyRes => {
      //console.log(JSON.stringify(RoyRes));
      if (RoyRes.length > 0) {

        Roy.MapsGlobelObject.NearByAgents = '';

        RoyRes.forEach(element => {

          if (Roy.MapsGlobelObject.NearByAgents === '') {
            Roy.MapsGlobelObject.NearByAgents = element.AgentID;
          }
          else {
            Roy.MapsGlobelObject.NearByAgents = Roy.MapsGlobelObject.NearByAgents + 'R' + element.AgentID;
          }

        });
      }
      else {
        alert("Agents are not available in your location. Inconvenience Regretted.");
        Roy.MapsGlobelObject.NearByAgents = null;
      }
    }, err => {


    })
  }


  GetETA(Seconds) {
    var hours: any = Math.floor(Seconds / 3600);
    var minutes: any = Math.floor((Seconds - (hours * 3600)) / 60);
    var seconds: any = Seconds - (hours * 3600) - (minutes * 60);

    if (hours < 10) { hours = "0" + hours; }
    if (minutes < 10) { minutes = "0" + minutes; }
    if (seconds < 10) { seconds = "0" + seconds; }
    return hours + 'h:' + minutes + 'm';
  }


  /* get route map between source and destination */
  DisplayRoute(directionsService, directionsDisplay, obj, map) {

    // Refresh the map again.. clear the positions
    map = new google.maps.Map(this.gmapElement.nativeElement, {
      zoom: 14,
    });

    directionsDisplay.setMap(map);
    directionsService.route({
      origin: { lat: obj.originlat, lng: obj.originlng },
      destination: { lat: obj.destinationlat, lng: obj.destinationlng },
      waypoints: [],
      optimizeWaypoints: true,
      travelMode: 'DRIVING'
    }, function (response, status) {
      if (status == 'OK') {
        directionsDisplay.setDirections(response);
        var myroute = directionsDisplay.directions.routes[0];
      } else {
        window.alert('Directions request failed due to ' + status);
      }
    });

  };

  fnAddClick(): void {

    let Roy = this;
    if (!Roy.MapsGlobelObject.Source) {
      alert("Please choose source");
      $("#txtSource").focus();
    }
    else if (!Roy.MapsGlobelObject.Destination) {
      alert("Please choose destination");
      $("#txtDestination").focus();
    }
    else if (!Roy.MapsGlobelObject.MobileNumber) {
      alert("Please enter sender mobile number");
      $("#MobileNumber").focus();
    }
    else if (!Roy.MapsGlobelObject.ReceiverMobileNumber) {
      alert("Please enter receiver mobile number");
      $("#ReceiverMobileNumber").focus();
    }
    else {
      Roy.fnClearItemInputs();
      $("#ItemsModal").modal("show");
    }
  }



  public SelectedItem(Item): void {
    let ROY = this;
    Item = JSON.parse(Item);
    ROY.objItems.ItemID = Item.ItemID;
    ROY.objItems.ItemName = Item.ItemName;
    //console.log(JSON.stringify(Item));
  }

  public SelectedQty(Qty): void {
    let ROY = this;
    ROY.objItems.Qty = Qty;

  }

  public SelectedWeight(objWeight): void {
    let ROY = this;
    objWeight = JSON.parse(objWeight);
    ROY.objItems.Weight_Qty = objWeight.Weight_Qty;
    ROY.objItems.Weight = objWeight.Weight;
  }

  fnAddItems(): void {
    let ROY = this;
    if (!ROY.objItems.ItemName) {
      alert("Please select item");
      $("#ddlItem").focus();
    }
    else if (!ROY.objItems.Qty) {
      alert("Please select quantity");
      $("#ddlQty").focus();
    }
    else if (!ROY.objItems.PValue) {
      alert("Please enter parcel value");
      $("#txtValue").focus();
    }
    else if (!ROY.objItems.Weight_Qty) {
      alert("Please select weight");
      $("#ddlWeight").focus();
    }
    else {

      // calculating Amount for Weight

      let AmountforWeight: any = 0;

      if (this.dtRates.length > 0) {
        let MinWeight: any = this.dtRates[0].Weight_gms;
        let WeightAmount: any = this.dtRates[0].WeightAmount;


        // convert kgs into grams
        let WeigtInGms = ROY.objItems.Weight * 1000;


        // get amount based on distance
        if (WeigtInGms <= MinWeight) {
          AmountforWeight = WeightAmount;
        }
        else if (WeigtInGms > MinWeight) {
          AmountforWeight = (WeigtInGms / MinWeight) * WeightAmount;
        }
        //console.log("Amount for weight : " + AmountforWeight)
        this.lblFare += Math.round(AmountforWeight);
      }


      // UOM ID   1===> gms , 2===> kgs
      ROY.dtSelectedItems.push({
        'ItemID': ROY.objItems.ItemID, 'ItemName': ROY.objItems.ItemName, 'Qty': ROY.objItems.Qty, 'CategoryID': 1, 'UOMID': 2,
        'ParcelValue': ROY.objItems.PValue, 'Weight_Qty': ROY.objItems.Weight_Qty, 'Weight': ROY.objItems.Weight, 'weightAmount': AmountforWeight, 'ItemDesc': ROY.objItems.Desc
      });
      // console.log("ROYYYYYYYYYYY : " + JSON.stringify(ROY.dtSelectedItems));


      //Get PromoCodes List
      if (ROY.dtSelectedItems.length == 1) { // Load promocodes at adding 1st item
        ROY.fnGetPromoCodesList();
        ROY.InvoiceShow = true;
        this.divPayUSubmit = true;
        this.divFreeRideSubmit = false;
      }

      // Clearing Inputs
      ROY.fnClearItemInputs();
    }
  }



  fnGetPromoCodesList(): void {

    let FareAmount = Math.round(this.lblFare);

    this.service.getwithId("api/PromoCode/", FareAmount).subscribe(res => {
      this.dtPromoCodes = res;
      // console.log(JSON.stringify(this.dtPromoCodes));
    }, err => {

    })
  }

  fnOpenPromoCodeModal(): void {
    this.fnGetPromoCodesList();
  }

  fnClearItemInputs(): void {
    let ROY = this;
    ROY.objItems = [];
    ROY.ddlQty = 0;
    ROY.ddlItem = 0;
    ROY.ddlWeight = 0;
  }


  fnRemoveItem(index): void {

    // Reduce item fare
    let ROY = this;
    let WeightAmount = ROY.dtSelectedItems[index].weightAmount;
    this.lblFare = this.lblFare - WeightAmount;

    // remove item
    ROY.dtSelectedItems.splice(index, 1);

    if (ROY.dtSelectedItems.length === 0) {
      this.lblPromoOffer = 0;
      this.lblTotalAmount = this.lblFare + this.lblTotCancelCharges - this.lblPromoOffer;
      this.txtPromoCode = null;
      this.ShowbtnApply = true;
      $("#txtPromoCode").prop('disabled', false);

      ROY.InvoiceShow = false;
      this.divPayUSubmit = false;
      this.divFreeRideSubmit = false;
    }

  }

  btnApplyClick(): void {
    if (!this.txtPromoCode) {
      alert("Please enter PromoCode");
      $("#txtPromoCode").focus();
    }
    else {

      let PromoElements: any = {};
      PromoElements = this.dtPromoCodes.filter(ROY => ROY.PromoCode.toUpperCase() === this.txtPromoCode.toString().toUpperCase());

      if (PromoElements.length > 0) {
        this.fnApplyPromoCode(PromoElements[0]);
      }
      else {
        alert("Invalid Promo Code");
        $("#txtPromoCode").focus();
      }
    }
  }


  fnApplyPromoCode(PromoElement): void {

    // if (!this.txtPromoCode) {
    //   alert("Please enter PromoCode");
    //   $("#txtPromoCode").focus();
    // }
    // else {
    //   this.ShowbtnApply = false;
    //   $("#txtPromoCode").attr("disabled", "disabled");
    // }
    let obj: any = {};
    obj.promoCode = PromoElement.PromoCode;
    obj.CustomerId = PromoElement.customerID;

    this.service.PostMethodWithHeaders(obj, "api/PromoCode").subscribe(res => {
      if (res.length > 0) {

        if (res[0].StatusCode == 44) { // elegible for offer

          if (parseInt(PromoElement.Discount_Percent) === 100) { // Free ride
            this.divFreeRideSubmit = true;
            this.divPayUSubmit = false;
          }

          this.txtPromoCode = PromoElement.PromoCode.toUpperCase();
          this.lblPromoOffer = (parseInt(PromoElement.Discount_Percent) * this.lblFare) / 100;  //parseFloat(PromoElement.Savings);
          this.lblTotalAmount = this.lblFare + this.lblTotCancelCharges - this.lblPromoOffer;
          this.ShowbtnApply = false;
          $("#txtPromoCode").prop('disabled', true);
          // $("#txtPromoCode").attr("disabled", "disabled");
          $("#PromoCodeModal").modal("hide");
        }
        else {
          alert(res[0].StatusMessage);
        }
      }

    }, err => {

    })
  }

  fnRemovePromoCode(): void {

    this.lblPromoOffer = 0.00;
    this.lblTotalAmount = this.lblFare + this.lblTotCancelCharges - this.lblPromoOffer;

    this.txtPromoCode = null;
    this.ShowbtnApply = true;
    this.divPayUSubmit = true;
    this.divFreeRideSubmit = false;
    $("#txtPromoCode").prop('disabled', false);
    $("#txtPromoCode").focus();
    // $("#txtPromoCode").removeAttr('disabled');
  }

  fnPOD(): void {
    this.isPayU = false;
    $("#PaymentConfirm").modal("show");
  }

  fnPayU(): void {
    this.isPayU = true;
    $("#PaymentConfirm").modal("show");
  }



  fnPaymentProceed(): void {

    if (this.isPayU) { // go through PayU gateway
      this.IsPaymentDone = true;
      this.fnConfirmBooking();
    }
    else { // POD
      this.IsPaymentDone = false;
      this.fnConfirmBooking();
    }

  }



  fnConfirmBooking() {

    let Roy = this;
    if (!Roy.MapsGlobelObject.Source) {
      alert("Please select source");
      $("#txtSource").focus();
    }
    else if (!Roy.MapsGlobelObject.Destination) {
      alert("Please select destination");
      $("#txtDestination").focus();
    }
    else if (!Roy.MapsGlobelObject.NearByAgents) {
      alert("Agents are currently not available. Inconvenience Regretted");
      $("#txtSource").focus();
    }
    else if (!Roy.MapsGlobelObject.MobileNumber) {
      alert("Please enter mobile number and retry");
      $("#MobileNumber").focus();
    }
    else if (!Roy.MapsGlobelObject.ReceiverMobileNumber) {
      alert("Please enter receiver mobile number and retry");
      $("#ReceiverMobileNumber").focus();
    }
    else if (Roy.dtSelectedItems.length === 0) {
      alert("Please add items to deliver");
    }
    else {

      this.fnSaveBooking();

    }
  }

  // Save Booking
  fnSaveBooking(): void {

    let Roy = this;

    let obj: any = {};

    var mm = new Date().getMonth() + 1;
    obj.BookingID = "EZ" + "-" + this.service.fnGetUniqueNo() + "-" + new Date().getDate() + mm;

    obj.Source = Roy.MapsGlobelObject.Source
    obj.Destination = Roy.MapsGlobelObject.Destination
    obj.SenderMobile = Roy.MapsGlobelObject.MobileNumber;
    obj.ReceiverMobile = Roy.MapsGlobelObject.ReceiverMobileNumber;
    obj.FromLat = Roy.MapsGlobelObject.FromLat;
    obj.FromLong = Roy.MapsGlobelObject.FromLong;
    obj.ToLat = Roy.MapsGlobelObject.ToLat;
    obj.ToLong = Roy.MapsGlobelObject.ToLong;
    obj.Distance_KM = Roy.lblDistance;
    obj.EstimatedTime = Roy.lblETA;
    obj.TotalFare = Roy.lblFare;
    obj.AgentID = 0; // Agent not alloted

    obj.CustomerID = Roy.customerID; //this.UserDetails[0].UserId;
    obj.BookingStatus = 0 // Pending
    obj.CancelledBy = 0;
    obj.NearByAgents = Roy.MapsGlobelObject.NearByAgents;

    var adressarray: any = obj.Source.split(",");
    adressarray = adressarray.reverse();
    obj.city = adressarray[2].trim();
    obj.areaName = adressarray[3].trim();

    obj.IsPaymentDone = Roy.IsPaymentDone;
    obj.Promocode = Roy.txtPromoCode;
    obj.DiscountAmount = Roy.lblPromoOffer;
    obj.TransactionID = "TR-" + obj.BookingID;


    // Booking Details Table
    obj.arrBookingDetails = [];

    Roy.dtSelectedItems.forEach(element => {
      var array: any = [];
      array.push(element.CategoryID);
      array.push(element.ItemID);
      array.push(element.Weight);
      array.push(element.Qty);
      array.push(element.ParcelValue);
      array.push(element.UOMID);
      if (element.ItemDesc !== undefined) {
        array.push(element.ItemDesc);
      } else {
        array.push(null);
      }
      obj.arrBookingDetails.push(array);
    });

    console.log(JSON.stringify(obj));

    //service for booking.
    this.service.PostMethodWithHeaders(obj, "api/UserBooking").subscribe(response => {

      alert(response);

    }, err => {

    })

  }



}
