import { Component, OnInit } from '@angular/core';
import { CustomServiceService } from '../../ez-services/custom-service.service';
import { Router } from "@angular/router";
import { BehaviorSubject } from 'rxjs';
// import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginObj:any={}
  private loggedIn = new BehaviorSubject<boolean>(false); // {1}
  httpLoader: boolean = false;

  constructor(private service: CustomServiceService,private router: Router) { }

  ngOnInit() {
  }

  get isLoggedIn() {
    return this.loggedIn.asObservable(); // {2}
  }

   //function for Password and press Enter key.
   UserPressOnEnter(event) {
    if (event.keyCode === 13) {
      this.Login();
    }

  }

  //function for Login.
  Login(){

    if (!this.loginObj.UserName) {
      alert("please enter user name");
    } else if (this.loginObj.password === undefined) {
      alert("please enter password");
    } else {

      this.httpLoader = true;

      let obj: any = {};

      obj.LoginID = this.loginObj.UserName;
      obj.UserPwd = this.loginObj.password;
    
      let url="api/Login";
      this.service.login(obj,url);
      this.httpLoader = false;
      // console.log(obj, url).subscribe()
    }

  }

  cancel(){
    // this.dialogRef.close();
  }
}
