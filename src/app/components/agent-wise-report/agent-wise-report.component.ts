import { Component, OnInit } from '@angular/core';
import { CustomServiceService } from '../../ez-services/custom-service.service';
import { HttpErrorResponse } from "@angular/common/http";
declare var $: any;
//import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-agent-wise-report',
  templateUrl: './agent-wise-report.component.html',
  styleUrls: ['./agent-wise-report.component.css']
})
export class AgentWiseReportComponent implements OnInit {

  dtAgents: any;
  AgentDetails: any = {};

  AgentregDetails: any = {};
  ctselectedValue: any = 0;
  dtbookingDetails: any = {};
  dtFreebookingDetails: any = {};
  txtAgentregSearch: any;
  lblrecordCount: any = 0;
  lblTotUserFare: any = 0;
  lblTotdistance: any = 0;
  lblTotComission: any = 0;

  lblFreeRecordCount: any = 0;
  lblFreeTotUserFare: any = 0;
  lblFreeTotdistance: any = 0;
  lblFreeTotComission: any = 0;

  pageNo: any;
  pageNo1: any;

  rbnAllchecked: any;

  rbnFreechecked: any



  //   public MOMENT_DATE_FORMATS: MatDateFormats = {
  //   parse: {
  //     dateInput: 'D/MM/YYYY'
  //   },
  //   display: {
  //     dateInput: 'DD/MM/YYYY',
  //     monthYearLabel: 'MMMM Y',
  //     dateA11yLabel: 'LL',
  //     monthYearA11yLabel: 'MMMM Y'
  //   }
  // }

  constructor(public service: CustomServiceService) { }


  ngOnInit() {

    this.GetAgentDetails();
    this.AgentDetails.txtFromDate = new Date().toISOString().split('T')[0];
    this.AgentDetails.txtToDate = new Date().toISOString().split('T')[0];

  }




  // Getting all the Agents
  GetAgentDetails() {

    let Url = "api/Agent";
    this.service.getmethod(Url).subscribe(response => {

      this.dtAgents = response.filter(Agent => Agent.IsActive === true);

    }, err => {

    })

  }

  fnAllRides() {

    this.GetDetails();
  }


  fnFreeRides() {

   


    this.lblFreeTotUserFare = 0;
    this.lblFreeTotComission = 0;
    this.lblFreeTotdistance = 0;


    let ViewAgentDetails: any = {};

    ViewAgentDetails.agentID = this.ctselectedValue;
    ViewAgentDetails.FromDate = this.AgentDetails.txtFromDate;
    ViewAgentDetails.ToDate = this.AgentDetails.txtToDate;

    this.service.PostMethodWithHeaders(ViewAgentDetails, 'api/AgentFreeRides').subscribe(res => {

      if (res.length > 0) {
        this.dtFreebookingDetails = res;
        console.log(res);
        this.lblrecordCount = this.dtFreebookingDetails.length;

        this.dtFreebookingDetails.forEach(element => {

          this.lblFreeTotUserFare += element.TotalFare;
          this.lblFreeTotdistance += element.Distance_KM;
          this.lblFreeTotComission += element.AgentComission;

        });

        $("#FreeRideModal").modal("show");
      }
      else {
        this.dtFreebookingDetails = {};
        alert("No Data Available")
      }


    }, (err: HttpErrorResponse) => {

      if (err.error instanceof Error) {
        console.log("client side error");
      }
      else {
        console.log("Server side error");
      }

    })

  }


  GetDetails() {
    this.rbnAllchecked = true;
    this.rbnFreechecked = false;

    if (!this.AgentDetails.txtFromDate) {
      alert("Please Select From Date");
      $("#txtFromDate").focus();

    }
    else if (!this.AgentDetails.txtToDate) {
      alert("Please Select To Date");
      $("#txtToDate").focus();
    }
    else if (this.AgentDetails.txtToDate < this.AgentDetails.txtFromDate) {
      alert("To Date Should be Greather than From Date");
      $("#txtToDate").focus();
    }
    else {

      this.lblTotUserFare = 0;
      this.lblTotComission = 0;
      this.lblTotdistance = 0;

      let ViewAgentDetails: any = {};

      ViewAgentDetails.agentID = this.ctselectedValue;
      ViewAgentDetails.FromDate = this.AgentDetails.txtFromDate;
      ViewAgentDetails.ToDate = this.AgentDetails.txtToDate;


      this.service.PostMethodWithHeaders(ViewAgentDetails, 'api/AgentwiseRegister').subscribe(res => {

        if (res.length > 0) {
          this.dtbookingDetails = res;
          console.log(res);
          this.lblrecordCount = this.dtbookingDetails.length;

          // let TotUserfare:any;

          // for (var i = 0; i < this.dtbookingDetails.length; i++) {
          //   TotUserfare
          // }

          this.dtbookingDetails.forEach(element => {
            this.lblTotUserFare += element.TotalFare;
            this.lblTotdistance += element.Distance_KM;
            this.lblTotComission += element.AgentComission;

          });
        }
        else {
          this.dtbookingDetails = {};
          alert("No Data Available")
        }


      }, (err: HttpErrorResponse) => {

        if (err.error instanceof Error) {
          console.log("client side error");
        }
        else {
          console.log("Server side error");
        }

      })
    }

  }



}



