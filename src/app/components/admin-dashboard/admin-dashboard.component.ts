import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { CustomServiceService } from '../../ez-services/custom-service.service';
import { HttpErrorResponse } from "@angular/common/http";



@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {

  data: any = {};

  width = 580;
  height = 400;
  dataFormat = 'json';
  ChartOptions: any;
  dtCity: any;
  ddlCitySelectedValue: any;
  ddlMonthSelectedValue: any;
  // dataSource: any;
  dtDeliverySummary: any;
  dtAreaWiseBookings: any;
  dtCityWiseBookings: any;
  dtMonthlyIncome: any;
  dtWeeklyIncome: any;
  dtMonths: any;
  @ViewChild('longestRouteMap') elemLongestRouteMap: any;
  @ViewChild('ShortestRouteMap') elemShortestRouteMap: any;
  lblLongSource: any;
  lblLongDestination: any;
  lblLongDistance: any;
  lblShortSource: any;
  lblShortDestination: any;
  lblShortDistance: any;

  txtSearch: any;
  txtusersearch: any;
  dtPresentAgents: any;
  lblrecordCount: any = 0;

  dtUsers: any;
  lbluserCount: any = 0;

  dtallAgents: any = [];
  lblagentsCount: any = 0;

  dtAgentRatting: any = {};

  dtWeekelyPayments: any = {};
  pageNo:any;
  pageNo1:any;
  pageNo2:any;


  // Map
  @ViewChild('gmap') gmapElement: any;
  navIsFixed: boolean;
  map: google.maps.Map;

  constructor(public service: CustomServiceService) {




    this.ChartOptions = {
      // "caption": "",
      // "subcaption": "For a net-worth of $1M",
      "captionFont": "calibri",
      "captionFontSize": "20",
      "captionFontColor": "#993300",
      "captionFontBold": "1",
      "useRoundEdges": "1",
      "labelDisplay": "rotate",
      "slantLabel": "1",
      "palettecolors": "#FFA500, #28c3be,#008001", /*colors for graph  */

      "baseFont": "calibri",
      "baseFontSize": "16",
      "baseFontColor": "#0066cc",
      "baseFontBold": "1",

      "showvalues": "1",
      "showpercentintooltip": "1", /*Percentage on tooltip*/
      // "numberprefix": "$",
      "enablemultislicing": "0",  /*auto close blocks when click on one block, if not '1'*/
      "useDataPlotColorForLabels": "1",
      // "plotHighlightEffect": "fadeout|anchorBgColor=ff0000, anchorBgAlpha=50",
      "showLegend": "1", /*To show labels on footer*/
      "interactiveLegend": "1", /*To Expand each label block when clicked the label , if not use '0' */
      "theme": "gammel",  /*Fusion or Gammel or Candy or Zune or Ocean*/

      "legendPosition": "bottom", /*Bottom or right for label position*/
      "legendAllowDrag": "1", /*To drag the legends one place to another , if not '0'*/
      "legendIconScale": "1.5", /*size for legend i.e. 1,2,3 .....*/
      "legendItemFontBold": "1",
      "legendItemFont": "calibri",
      "legendItemFontSize": "12",
      "legendItemFontColor": "#30328a",
      // "reverseLegend": "1" /*To change the order of legends*/

      // "canvasBorderThickness": "1",
      // "showAlternateHGridColor": "0",
      // "bgColor": "#eeeeee",
      // "canvasbgAlpha": "100",
      // "canvasBgRatio": "40,60",
      // "canvasBgAngle": "0"

    }

  }

  ngOnInit() {
    this.TotalDeliverySummary();
    this.CityWiseBookings();
    this.MonthlyIncomePayments();
    this.LongestRouteTravelled();
    this.ShortestRouteTravelled();
    this.GetAvailableAgents();
    this.GetallAgents();
    this.GetAllSubscribers();
    this.GetAgentTopRating();


    this.AgentLocations();
    //this.loadMap();
  }



  TotalDeliverySummary() {

    // this.ChartOptions.caption="Total Delivery Summary";

    // Passing Graph Caption to ChartOptions
    let myobj: any = Object.assign({}, this.ChartOptions);
    myobj.caption = "Total Delivery Summary";


    let obj: any = {};
    this.service.PostMethodWithHeaders(obj, "api/AgentDeliverySummary").subscribe(response => {

      if (response.length > 0) {

        this.dtDeliverySummary = {
          "chart": myobj,
          "data": [
            {
              "label": "Today",
              "value": response[0].TodayDeliveries
            },
            {
              "label": "Weekly",
              "value": response[0].WeekDeliveries
            },
            {
              "label": "Monthly",
              "value": response[0].MonthDeliveries
            }
          ]
        }
      }

    }, err => {

    })

  }


  CityWiseBookings() {

    let myobj: any = Object.assign({}, this.ChartOptions);
    myobj.caption = "City Wise Bookings";

    this.service.getmethod("api/CityWiseBooking").subscribe(response => {

      if (response.length > 0) {
        this.dtCity = response;
        // console.log(this.dtCity);
        var bardata: any = [];
        for (var i = 0; i < response.length; i++) {

          bardata.push({
            'label': response[i].city, 'value': response[i].TotalBookings
          });

        }

        this.dtCityWiseBookings = {
          "chart": myobj,
          "data": bardata
        }

        // display 1st city area wise records
        this.ddlCitySelectedValue = this.dtCity[0].SNo;
        // this.AreaWiseBookings('"' + response[0].city + '"');
        this.AreaWiseBookings(response[0].city);

      }

    }, err => {

    })

  }


  OnCityChange(citySno: any) {
    // alert(citySno);
    var cityDetails: any = this.dtCity.filter(Roy => Roy.SNo.toString() === citySno.toString());
    //  console.log(cityDetails[0].city);
    this.AreaWiseBookings(cityDetails[0].city);

  }

  AreaWiseBookings(city: string) {

    // var yourString = ""Hyderabad"";
    // var city = city.substring(1, city.length - 1); // removing 1st and last character

    let myobj: any = Object.assign({}, this.ChartOptions);
    myobj.caption = "Area Wise Bookings";

    // this.ChartOptions.caption="City Wise Bookings";

    this.service.getwithId("api/AreaWiseBooking", city).subscribe(response => {

      if (response.length > 0) {
        var bardata: any = [];
        for (var i = 0; i < response.length; i++) {

          bardata.push({
            'label': response[i].AreaName, 'value': response[i].TotalBookings
          });

        }

        this.dtAreaWiseBookings = {
          "chart": myobj,
          "data": bardata
        }
      }

    }, err => {

    })

  }


  // Getting Monthly Income , Payments , Profits
  MonthlyIncomePayments() {

    let myobj: any = Object.assign({}, this.ChartOptions);
    myobj.caption = "Monthly Income / Payment / Profit";
    myobj.showpercentintooltip = "0";
    myobj.theme = "Candy";
    myobj.palettecolors = "#DAA520, #66CDAA,#FFB6C1";
    myobj.showvalues = "0";


    this.service.getmethod("api/MonthlyIncomePayments").subscribe(response => {
      if (response.length > 0) {


        this.dtMonths = response;


        var categories: any = [];
        var seriesname_Income: any = [];
        var seriesname_Payment: any = [];
        var seriesname_Profit: any = [];



        for (var i = 0; i < response.length; i++) {

          categories.push({
            'label': response[i].MonthName
          });

          seriesname_Income.push({
            'value': response[i].Income
          })

          seriesname_Payment.push({
            'value': response[i].Payment
          })

          seriesname_Profit.push({
            'value': response[i].Profit
          })

        }

        this.dtMonthlyIncome = {

          "chart": myobj,

          "categories": [
            {
              "category": categories
            }
          ],
          "dataset": [
            {
              "seriesname": "Income",
              "data": seriesname_Income
            },
            {
              "seriesname": "Payment",
              "data": seriesname_Payment
            },
            {
              "seriesname": "Profit",
              "data": seriesname_Profit
            }
          ]
        }

        this.ddlMonthSelectedValue = this.dtMonths[0].Month;
        this.WeekelyIncomePayments(this.dtMonths[0].Month);

      }
    }, err => {

    })


  }


  OnMonthChange(MonthNo: any) {

    //alert(MonthNo);
    this.WeekelyIncomePayments(MonthNo);

  }

  // Weekely Income , Payments, Profits
  WeekelyIncomePayments(MonthNo: any) {


    let myobj: any = Object.assign({}, this.ChartOptions);
    myobj.caption = "Weekly Income / Payment / Profit";
    myobj.showpercentintooltip = "0";
    myobj.theme = "Candy";
    // myobj.palettecolors = "#DAA520, #66CDAA,#FFB6C1";
    myobj.showvalues = "0";

    // this.ChartOptions.caption="City Wise Bookings";

    this.service.getwithId("api/WeekelyIncomePayments", MonthNo).subscribe(response => {
      // console.log(response);

      if (response.length > 0) {

        var categories: any = [];
        var seriesname_Income: any = [];
        var seriesname_Payment: any = [];
        var seriesname_Profit: any = [];


        for (var i = 0; i < response.length; i++) {

          categories.push({
            'label': 'Week ' + response[i].WeekNo.toString()
          });

          seriesname_Income.push({
            'value': response[i].Income
          })

          seriesname_Payment.push({
            'value': response[i].Payment
          })

          seriesname_Profit.push({
            'value': response[i].Profit
          })

        }

        this.dtWeeklyIncome = {

          "chart": myobj,

          "categories": [
            {
              "category": categories
            }
          ],
          "dataset": [
            {
              "seriesname": "Income",
              "data": seriesname_Income
            },
            {
              "seriesname": "Payment",
              "data": seriesname_Payment
            },
            {
              "seriesname": "Profit",
              "data": seriesname_Profit
            }
          ]
        }

      }
    }, err => {

    })

  }



  // Getting the Longest Route Travelled by the Agent
  LongestRouteTravelled(): void {
    this.service.getmethod("api/LongestRouteBooking").subscribe(response => {
      //  console.log("Longest Route : " + JSON.stringify(response));

      if (response.length > 0) {

        this.lblLongSource = response[0].Source;
        this.lblLongDestination = response[0].Destination;
        this.lblLongDistance = response[0].Distance_KM;

        var objMap: any = {};
        objMap.originlat = parseFloat(response[0].FromLat);
        objMap.originlng = parseFloat(response[0].FromLong);
        objMap.destinationlat = parseFloat(response[0].ToLat);
        objMap.destinationlng = parseFloat(response[0].ToLong);


        this.DisplayRoute(objMap, this.elemLongestRouteMap.nativeElement);
      }
    }, err => {

    })
  }

  // Getting the Shortest Route Travelled by the Agent
  ShortestRouteTravelled(): void {
    this.service.getmethod("api/ShortestRouteBooking").subscribe(response => {
      // console.log("Shortest Route : " + JSON.stringify(response));
      if (response.length) {

        this.lblShortSource = response[0].Source;
        this.lblShortDestination = response[0].Destination;
        this.lblShortDistance = response[0].Distance_KM;



        var objMap: any = {};
        objMap.originlat = parseFloat(response[0].FromLat);
        objMap.originlng = parseFloat(response[0].FromLong);
        objMap.destinationlat = parseFloat(response[0].ToLat);
        objMap.destinationlng = parseFloat(response[0].ToLong);


        this.DisplayRoute(objMap, this.elemShortestRouteMap.nativeElement);
      }
    }, err => {

    })
  }

  DisplayRoute(obj, MapDivID: any) {
    var map: google.maps.Map;
    map = new google.maps.Map(MapDivID, {
      zoom: 14
    });
    var directionsDisplay: any = new google.maps.DirectionsRenderer({
      draggable: false,
      map: map
    });

    var directionsService: any = new google.maps.DirectionsService;

    directionsDisplay.setMap(map);
    directionsService.route({
      origin: { lat: obj.originlat, lng: obj.originlng },  // Haight.
      destination: { lat: obj.destinationlat, lng: obj.destinationlng },  // Ocean Beach.
      waypoints: [],
      optimizeWaypoints: true,
      travelMode: 'DRIVING'
    }, function (response, status) {
      if (status == 'OK') {
        directionsDisplay.setDirections(response);
        var myroute: any = directionsDisplay.directions.routes[0];
      } else {
        window.alert('Directions request failed due to ' + status);
      }
    });

  }


  // Getting Present delivery Serving Agents
  GetAvailableAgents() {
    this.service.getmethod("api/PresentDeliveryServingAgents").subscribe(response => {

    
      if (response.length > 0) {
        this.dtPresentAgents = response
        // console.log(response);
        this.lblrecordCount = this.dtPresentAgents.length;
      }
      else {
        this.dtPresentAgents = [];
        // alert("No Data Available")
      }


    }, (err: HttpErrorResponse) => {

      if (err.error instanceof Error) {
        console.log("client side error");
      }
      else {
        console.log("Server side error");
      }


    })
  }

  // Getting Present Subscribers
  GetAllSubscribers() {
    this.service.getmethod("api/UserReg").subscribe(response => {


      if (response.length > 0) {
        this.dtUsers = response.filter(User => User.UserTypeId === 3);

        // console.log(response);
        this.lbluserCount = this.dtUsers.length;
      }
      else {
        this.dtUsers = [];
        // alert("No Data Available")
      }


    }, (err: HttpErrorResponse) => {

      if (err.error instanceof Error) {
        console.log("client side error");
      }
      else {
        console.log("Server side error");
      }


    })
  }

  // Getting all Agents
  GetallAgents() {
    this.service.getmethod("api/Agent").subscribe(response => {


      if (response.length > 0) {
        this.dtallAgents = response.filter(Agent => Agent.IsActive === true);

        //  console.log(this.dtallAgents);
        this.lblagentsCount = this.dtallAgents.length;
      }
      else {
        this.dtallAgents = [];
        // alert("No Data Available")
      }


    }, (err: HttpErrorResponse) => {

      if (err.error instanceof Error) {
        console.log("client side error");
      }
      else {
        console.log("Server side error");
      }


    })
  }

  // Get Agent Top Rating

  GetAgentTopRating() {
    let myobj: any = Object.assign({}, this.ChartOptions);
    myobj.caption = "Top Rating Agents";

    this.service.getmethod("api/AgentTopAvRating").subscribe(response => {

      this.dtAgentRatting = response;
      // console.log(this.dtAgentRatting);

      var agentbardata: any = [];
      for (var i = 0; i < response.length; i++) {

        agentbardata.push({
          'label': response[i].AgentName, 'value': response[i].AvgRating
        });

      }

      this.dtAgentRatting = {
        "chart": myobj,
        "data": agentbardata
      }

      // display 1st city area wise records
      // this.AreaWiseBookings('"' + response[1].city + '"');

    }, err => {

    })
  }

  // Map Zoom
  @HostListener("window:scroll", [])
  onWindowScroll() {
    if (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop > 100) {
      this.navIsFixed = true;
    } else if (this.navIsFixed && window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop < 10) { this.navIsFixed = false; }
  } scrollToTop() {
    (function smoothscroll() {
      var currentScroll = document.documentElement.scrollTop || document.body.scrollTop; if (currentScroll > 0) {
        window.requestAnimationFrame(smoothscroll);
        window.scrollTo(0, currentScroll - (currentScroll / 5));
      }
    })();
  }


  loadMap(dtAgentLocations: any) {


    console.log(dtAgentLocations);
    navigator.geolocation.getCurrentPosition((position) => {

      this.map = new google.maps.Map(this.gmapElement.nativeElement, {
        zoom: 12,
        center: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
        styles: [
          {
            "featureType": "administrative",
            "elementType": "labels",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "featureType": "administrative",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "featureType": "landscape.man_made",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "featureType": "poi",
            "elementType": "labels.icon",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "featureType": "poi",
            "elementType": "labels.text",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "featureType": "poi.park",
            "stylers": [
              {
                "color": "#d6f1c0"
              }
            ]
          },
          {
            "featureType": "road",
            "stylers": [
              {
                "visibility": "simplified"
              }
            ]
          },
          {
            "featureType": "water",
            "elementType": "geometry.fill",
            "stylers": [
              {
                "color": "#afdaf3"
              }
            ]
          }
        ],
        // center: { lat: parseFloat( dtAgentLocations[i].Latitude), lng: parseFloat( dtAgentLocations[i].longitude) }
      });

      var infowindow = new google.maps.InfoWindow();

      var marker, i;

      for (i = 0; i < dtAgentLocations.length; i++) {

        let markerLabel = {
          text: dtAgentLocations[i].AgentName,
          color: 'royalblue',
          fontWeight: 'bold',
        },


          marker = new google.maps.Marker({
            position: new google.maps.LatLng(parseFloat(dtAgentLocations[i].Latitude), parseFloat(dtAgentLocations[i].longitude)),
            map: this.map,
            label: markerLabel,
            icon: {
              labelOrigin: new google.maps.Point(11, 50),
              url: './assets/images/TopBikeIconSVG.svg',
              scaledSize: {
                width: 30,
                height: 30
              },
              size: new google.maps.Size(75, 75),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(11, 40),
            },
          });


        google.maps.event.addListener(marker, 'click', (function (marker, i) {
          return function () {
            infowindow.setContent(dtAgentLocations[i].location);
            infowindow.open(this.map, marker);
          }
        })(marker, i));
      }

    });
  }

  // Get Agent Locations

  AgentLocations() {

    this.service.getmethod("api/AgentLocations").subscribe(response => {
      if(response.length>0){
        this.loadMap(response);
      }
    
    }, err => {

    })

  }


}

