import { Component, OnInit } from '@angular/core';
import { CustomServiceService } from '../../ez-services/custom-service.service';
import { HttpErrorResponse } from "@angular/common/http";
import { iterateListLike } from '@angular/core/src/change_detection/change_detection_util';
import * as $ from 'jquery';

@Component({
  selector: 'app-item-master',
  templateUrl: './item-master.component.html',
  styleUrls: ['./item-master.component.css']
})
export class ItemMasterComponent implements OnInit {

  objItems: any = {};
  dtCategory: any = [];
  dtItems: any = [];
  dtUom: any = [];
  ctselectedValue: any = 0;
  UomselectedValue: any = 0;
  isEdit: boolean = false;
  txtSearch: any;
  btnText: any;
  httpLoader: boolean = false;
  pageNo:any;


  constructor(public _serv: CustomServiceService) { }

  // page initiation
  ngOnInit() {
    this.httpLoader = true;
    this.objItems.chkActive = false;
    this.btnText = "Save";
    this.GetCategories();
    this.GetUOM();
    this.GetItemList();
    this.httpLoader = false;
  }


  /* get all categories */
  private GetCategories(): void {

    this._serv.getmethod("api/CategoryInsert")
      .subscribe(res => {

        //this.dtCategory = res;
        // filter the data i.e getting active categories
        this.dtCategory = res.filter(SR => SR.IsActive === true);

      },
        (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
            console.log("client side error", err);
          }
          else {
            console.log("Server side error");
          }
        })

  }

  // /* Get Selected category object */
  // private SelectedCategory(objCategory) {
  //   //console.log(objCategory);
  //   this.objItems.CategoryID = JSON.parse(objCategory).CategoryID;

  // }



  // Get All Items list
  private GetItemList(): void {

    this._serv.getmethod("api/itemDetails")
      .subscribe(res => {
        this.dtItems = res;
        //console.log(this.dtItems);
      },
        (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
            console.log("client side error", err);
          }
          else {
            console.log("Server side error");
          }
        })

  }

  private GetUOM(): void {
    this._serv.getmethod("api/UOM")
      .subscribe(res => {
        this.dtUom = res;
        //console.log(JSON.stringify(this.dtUom));
      },
        (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
            console.log("client side error", err);
          }
          else {
            console.log("Server side error");
          }
        })

  }

  // // get all UOM details
  // private SelectedUOM(UomID): void {

  //   this.objItems.UomID = UomID;

  // }

  private fnEditItem(row): void {
    //console.log(JSON.stringify(row));
    this.objItems.txtItem = row.ItemName;
    this.ctselectedValue = row.CategoryID;
    this.objItems.txtWeight = row.Weight;
    this.UomselectedValue = row.UOMid;
    this.objItems.txtDesc = row.Description;
    this.objItems.chkActive = row.IsActive;

    this.objItems.ItemID = row.ItemID;
    this.isEdit = true;
    this.btnText = "Update";
  }

  public SaveItem(): void {
    //console.log("trher", this.objItems);
    // initializing field values to object

    if (!this.objItems.txtItem) {
      alert("Please enter Item");
      $("#txtItem").focus();
    }
    else if (!this.ctselectedValue) {
      alert("please select category");
      $("#ddlCategory").focus();
    }
    else if (!this.objItems.txtWeight) {
      alert("please enter weight");
      $("#txtWeight").focus();
    }
    else if (!this.UomselectedValue) {
      alert("please select UOM");
      $("#ddlUOM").focus();
    }
    else {

      this.httpLoader = true;

      let objApiParam: any = {};

      objApiParam.ItemName = this.objItems.txtItem;
      objApiParam.CategoryID = this.ctselectedValue; //this.objItems.CategoryID;
      objApiParam.Weight = this.objItems.txtWeight;
      objApiParam.UOMid = this.UomselectedValue; //this.objItems.UomID;
      objApiParam.Description = this.objItems.txtDesc;
      objApiParam.IsActive = this.objItems.chkActive;
      let url: string = "";
      if (this.isEdit == true) {
        objApiParam.itemID = this.objItems.ItemID;
        url = "api/ItemPut";
      }
      else {

        url = "api/itemDetails";
      }

      // calling Post API
      this._serv.PostMethodWithHeaders(objApiParam, url)
        .subscribe(res => {
          this.GetItemList();
          this.Cancel();

        }, (err: HttpErrorResponse) => {
          this.httpLoader = false;
        })

      this.httpLoader = false;
    }


  }


  public Cancel(): void {
    this.objItems = {};
    this.isEdit = false;
    this.UomselectedValue = 0;
    this.ctselectedValue = 0;
    this.btnText = "Save";
  }

  private fnInActiveItem(itemID): void {

    this.httpLoader = true;
    this._serv.getwithId("api/ItemPut", itemID).subscribe(res => {
      this.GetItemList();
      alert("in Active");
      this.httpLoader = false;
    }, (err: HttpErrorResponse) => {
      this.httpLoader = false;
    })

  }


}
