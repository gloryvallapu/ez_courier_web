import { Component, OnInit } from '@angular/core';
import { CustomServiceService } from '../../ez-services/custom-service.service';
import { HttpErrorResponse } from "@angular/common/http";
declare var $:any;
@Component({
  selector: 'app-userbooking-register',
  templateUrl: './userbooking-register.component.html',
  styleUrls: ['./userbooking-register.component.css']
})
export class UserbookingRegisterComponent implements OnInit {

  UserBooking: any = {};
  dtUserbookingDetails: any = {};
  txtUSerRegSearch: any;
  dtViewUserdetails: any = {};
  lbluserIDID: any = "";
  dtViewbookingDetails: any = {};
  dtFreebookingDetails: any = {};

  lblCustomerName: any = "";
  lblrecordCount: any = 0
  txtsearch: any;
  pageNo: any;
  pageNo1: any;

  lbltotdistance: any = 0;
  lbltotUserfare: any = 0;
  lbltotagentfare: any = 0;

  lbltotdis: any = 0;
  lbltotamt: any = 0;
  lbltotageamount: any = 0;

  lblFreeTotUserFare: any = 0;
  lblFreeTotComission: any = 0;
  lblFreeTotdistance: any = 0;
  
  constructor(public service: CustomServiceService) { }

  ngOnInit() {
    this.UserBooking.txtFromDate = new Date().toISOString().split('T')[0];
    this.UserBooking.txtToDate = new Date().toISOString().split('T')[0];
  }

  GetUserDetails() {
    if (!this.UserBooking.txtFromDate) {
      alert("Please Select From Date");
      $("#txtFromDate").focus();
    }
    else if (!this.UserBooking.txtToDate) {
      alert("Please Select To Date");
      $("#txtToDate").focus();
    }
    else {

      this.lbltotdistance = 0;
      this.lbltotUserfare = 0;
      this.lbltotagentfare = 0;

      let ViewUserDetails: any = {};
      ViewUserDetails.FromDate = this.UserBooking.txtFromDate;
      ViewUserDetails.ToDate = this.UserBooking.txtToDate;


      this.service.PostMethodWithHeaders(ViewUserDetails, 'api/UserBookingSummary').subscribe(res => {
        //console.log(res);
        if (res.length > 0) {
          this.dtUserbookingDetails = res;
          this.lblrecordCount = this.dtUserbookingDetails.length;

          this.dtUserbookingDetails.forEach(element => {
            this.lbltotdistance += element.Distance;
            this.lbltotUserfare += element.UserAmount;
            this.lbltotagentfare += element.AgentAmount;

          });
        }
        else {
          this.dtUserbookingDetails = {};
          alert("No Data Available")
        }


      }, (err: HttpErrorResponse) => {

        if (err.error instanceof Error) {
          console.log("client side error");
        }
        else {
          console.log("Server side error");
        }

      })
    }

  }

  // Free Rides
  fnFreeRides() {

  


    this.lblFreeTotUserFare = 0;
    this.lblFreeTotComission = 0;
    this.lblFreeTotdistance = 0;


    let ViewUserDetails: any = {};

    ViewUserDetails.FromDate = this.UserBooking.txtFromDate;
    ViewUserDetails.ToDate = this.UserBooking.txtToDate;

    this.service.PostMethodWithHeaders(ViewUserDetails, 'api/UserFreeRides').subscribe(res => {

      if (res.length > 0) {
        $("#FreeRideModal").modal("show");
        this.dtFreebookingDetails = res;
        //console.log(res);
        this.lblrecordCount = this.dtFreebookingDetails.length;

        this.dtFreebookingDetails.forEach(element => {

          this.lblFreeTotUserFare += element.UserAmount;
          this.lblFreeTotdistance += element.Distance;
          this.lblFreeTotComission += element.AgentAmount;

        });
      }
      else {
        this.dtFreebookingDetails = {};
        alert("No Data Available")
      }


    }, (err: HttpErrorResponse) => {

      if (err.error instanceof Error) {
        console.log("client side error");
      }
      else {
        console.log("Server side error");
      }

    })

  }


  // To view Agent wise PaymentDetails

  // GetSummaryDetails(CustomerID) {
  GetSummaryDetails(row) {

    this.lbltotdis = 0;
    this.lbltotamt = 0;
    this.lbltotageamount = 0;

    let ViewUserDetails: any = {};
    ViewUserDetails.userID = row.CustomerID;
    ViewUserDetails.FromDate = this.UserBooking.txtFromDate;
    ViewUserDetails.ToDate = this.UserBooking.txtToDate;
    //console.log(JSON.stringify(ViewUserDetails));
    this.service.PostMethodWithHeaders(ViewUserDetails, 'api/UserBookingSummaryDetails').subscribe(res => {
      console.log(JSON.stringify(res));
      if (res.length > 0) {
        this.lblCustomerName = row.CustomerName;
        this.dtViewbookingDetails = res;

        this.dtViewbookingDetails.forEach(element => {
          this.lbltotdis += element.Distance_KM;
          this.lbltotamt += element.TotalFare;
          this.lbltotageamount += element.AgentComission;

        });


        // console.log(this.lblCustomerName);
        // console.log(row.CustomerName);
        // console.log(row.CustomerID);
        // console.log(ViewUserDetails);
      }
      else {
        this.dtViewbookingDetails = {};
        alert("No Data Available")
      }
    }, (err: HttpErrorResponse) => {

      if (err.error instanceof Error) {
        console.log("client side error");
      }
      else {
        console.log("Server side error");
      }

    })


  }


}
