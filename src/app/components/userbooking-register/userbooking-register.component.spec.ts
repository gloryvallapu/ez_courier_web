import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserbookingRegisterComponent } from './userbooking-register.component';

describe('UserbookingRegisterComponent', () => {
  let component: UserbookingRegisterComponent;
  let fixture: ComponentFixture<UserbookingRegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserbookingRegisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserbookingRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
