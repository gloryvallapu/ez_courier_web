import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { CustomServiceService } from '../../ez-services/custom-service.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA,MatDialogConfig } from '@angular/material';
import { LoginComponent } from '../../components/login/login.component';
import { RegisterComponent } from '../../components/register/register.component';
import {Router} from '@angular/router';
import * as $ from 'jquery';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css',
             './signin-signup.component.css']
})
export class NavbarComponent implements OnInit {
  isLoggedIn$: Observable<boolean>;
  navbarObj:any={};
  constructor(private Service: CustomServiceService, public dialog: MatDialog,public route:Router) { }

  ngOnInit() {
   
    if (localStorage.getItem("UserDetails") !== null) {
    
      this.Service.loggedIn.next(true);
      this.isLoggedIn$ = this.Service.isLoggedIn;
    }else
    this.isLoggedIn$ = this.Service.isLoggedIn; // {2}
  }

  
  openLoginPopUp(arg) {
    // this.navbarObj.opendilog=arg;
    // console.log("open login");
    console.log(arg)
   //this.dialogRef.close();
   const dialogConfig = new MatDialogConfig();

        dialogConfig.disableClose = true;
        //dialogConfig.autoFocus = true;
    let dialogRef = this.dialog.open(LoginComponent, {
      disableClose: true,
      data: { opendilog: arg }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed',result);

    });
    
  }

  //function for logout.
  logOut(){
    console.log("logout");
    localStorage.clear();
    this.Service.loggedIn.next(false);
    this.isLoggedIn$ = this.Service.isLoggedIn;
  }
 //  Show/Hide landing Page menu
  MenuToggleClick(){
        $("#eznav").toggle();
   
  }
  MenuToggleInner(){
     $(".sub_toggle").toggle();
  }
  MenuToggleInner2(){
   $(".sub_toggle2").toggle();

   }
  openRegisterationPopUp(){

    // let dialogRef = this.dialog.open(RegisterComponent, {
    //   disableClose: true,
    //   width: '350px',
    //   height:'350px'
    // });

    // dialogRef.afterClosed().subscribe(result => {
    //   console.log('The dialog was closed',result);
    // });
  }

//   showMoboilNav() {
//     var x =<HTMLInputElement>document.getElementById('eznav');
//     if (x.className === "navbar-nav") {
//         x.className += " responsive";
//     } else {
//         x.className = "topnav";
//     }
// }

}


