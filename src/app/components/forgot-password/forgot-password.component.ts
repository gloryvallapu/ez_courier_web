import { Component, OnInit } from '@angular/core';
import { CustomServiceService } from '../../ez-services/custom-service.service';
import { HttpErrorResponse } from "@angular/common/http";
import * as crypto from "crypto-js";
import { Observable } from 'rxjs/Observable';
import * as $ from 'jquery';
import { Router } from '@angular/router';


@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  txtmobile: any = '';
  httpLoader: boolean = false;
  decPwd: string = "";
  key = "S8W8A8M5I1R8O7Y4N8A8NI";

  constructor(private service: CustomServiceService, private router: Router) { }

  ngOnInit() {


  }

  // Forgot Password
  ForgotPassword() {

    let obj: any = {};

    obj.LoginID = this.txtmobile;

    this.service.PostMethodWithHeaders(obj, 'api/Login').subscribe(res => {
      //console.log(res);


      if (res.length > 0) {

        let encPwd = res[0].UserPwd;
        let decPwd = crypto.DES.decrypt(encPwd, this.key).toString(crypto.enc.Utf8);
        this.SendOTP(this.txtmobile, decPwd);

      }
      else {
        //  this.loggedIn.next(false);
        alert("Invalid LoginID");
        $("#txtmobile").focus();
      }



    }, (err: HttpErrorResponse) => {

      if (err.error instanceof Error) {
        console.log("client side error");
      }
      else {
        console.log("Server side error");
      }
      this.httpLoader = false;
    })

  }



// Sending Password to Mobile
  public SendOTP(mobile, password): void {

    let Message = 'Your Password is ' + password;
    let objsms = { 'mobileNo': mobile, 'message': Message };
    // console.log(this.Password);
    this.service.PostMethodWithHeaders(objsms, "api/SendSMS").subscribe(response => {

      alert("Your Password is sent to your registered mobile");
      //this.regHide = true;
      //  $("#txtOTP").focus();
      this.txtmobile=null;
     // this.router.navigateByUrl('/home');

    }, err => {
      // this.regHide = false;

    })
  }



}
