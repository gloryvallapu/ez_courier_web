import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RefundOrdersComponent } from './refund-orders.component';

describe('RefundOrdersComponent', () => {
  let component: RefundOrdersComponent;
  let fixture: ComponentFixture<RefundOrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RefundOrdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RefundOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
