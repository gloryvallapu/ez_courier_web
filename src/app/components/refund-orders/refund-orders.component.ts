import { Component, OnInit } from '@angular/core';
import { CustomServiceService } from '../../ez-services/custom-service.service';
import { HttpErrorResponse } from "@angular/common/http";
// import { $ } from 'protractor';
declare var $: any;

@Component({
  selector: 'app-refund-orders',
  templateUrl: './refund-orders.component.html',
  styleUrls: ['./refund-orders.component.css']
})
export class RefundOrdersComponent implements OnInit {

  dtRefundOrders: any = {};
  txtRefundsearch: any;
  pageNo: any;
  lblrecordCount: any = 0
  txtviewpaymentsearch: any;
  dtViewPaymentdetails: any;
  refundAmt: any = 0;
  bookingID: any = '';

  lblTotFare: any = 0;
  lblTotdiscamount: any = 0;
  lblTotcanamount: any = 0;

  constructor(public service: CustomServiceService) { }

  ngOnInit() {
    this.GetRefundOrders();
  }

  // getting all the refund orders
  GetRefundOrders() {
    this.lblTotFare = 0;
    this.lblTotdiscamount = 0;
    this.lblTotcanamount = 0;

    let url = "api/RefundOrders";
    this.service.getmethod(url).subscribe(response => {
      // console.log(response);
      this.dtRefundOrders = response;
      this.lblrecordCount = this.dtRefundOrders.length;

      this.dtRefundOrders.forEach(element => {
        this.lblTotFare += element.TotalFare;
        this.lblTotdiscamount += element.DiscountAmount;
        this.lblTotcanamount += element.CancellationCharges;

      });

    }, (error: HttpErrorResponse) => {
      if (error.error instanceof Error) {
        console.log("client side error");
      }
      else {
        console.log("Server side error");
      }

    })
  }

  fnOpenModal(row): void {
    // console.log(row);
    this.refundAmt = row.RefundAmount;
    this.bookingID = row.BookingID;
    $('#RefundModal').modal("show");

  }


  fnPayRefundAmount(): void {
    let objParams: any = {};
    objParams.RefundAmount = this.refundAmt;
    objParams.BookingID = this.bookingID;
    console.log(JSON.stringify(objParams));
    this.service.PostMethodWithHeaders(objParams, "api/PayRefundAmount").subscribe(res => {

      this.GetRefundOrders();
      alert("Refund amount paid");
      $('#RefundModal').modal("hide");

    }, (error: HttpErrorResponse) => {
      if (error.error instanceof Error) {
        console.log("client side error");
      }
      else {
        console.log("Server side error");
      }

    })
  }



}
