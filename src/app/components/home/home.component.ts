import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { CustomServiceService } from '../../ez-services/custom-service.service';
import { } from "googlemaps";


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {


  @ViewChild('gmap') gmapElement: any;
  navIsFixed: boolean;
  MapsGlobelObject: any = {};
  map: google.maps.Map;
  bookIds: any;
  Login:boolean=true;
  register: boolean = false;
  fgPwd: boolean = false;
  ez_r_open: boolean = true;
  constructor(public service: CustomServiceService) { }

  ngOnInit() {
    this.loadMap();
  }
  ezbookingToggle() {
    this.ez_r_open = !this.ez_r_open;
  }


  // Map Zoom
  @HostListener("window:scroll", [])
  onWindowScroll() {
    if (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop > 100) {
      this.navIsFixed = true;
    } else if (this.navIsFixed && window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop < 10) { this.navIsFixed = false; }
  } scrollToTop() {
    (function smoothscroll() {
      var currentScroll = document.documentElement.scrollTop || document.body.scrollTop; if (currentScroll > 0) {
        window.requestAnimationFrame(smoothscroll);
        window.scrollTo(0, currentScroll - (currentScroll / 5));
      }
    })();
  }



  reg(reg) {
    if (reg === 1) {

      this.fgPwd = false;
      this.register = false;
      this.Login = true;

    } else if (reg === 2) {

      this.fgPwd = false;
      this.Login = false;
      this.register = true;
    }
    else if (reg === 3) {
      this.register = false;
      this.Login = false;
      this.fgPwd = true;
     
    }
  }

  loadMap() {
    navigator.geolocation.getCurrentPosition((position) => {
      var myLatlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      var mapProp = {
        center: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
        zoom: 18,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
      };
      var marker = new google.maps.Marker({
        position: myLatlng,
        draggable: true,
        title: "Hello World!"
      });
      this.map = new google.maps.Map(this.gmapElement.nativeElement, {
        zoom: 14,
        center: { lat: position.coords.latitude, lng: position.coords.longitude }
      });
      //marker.setMap(this.map);
    });
  }


}
