import { Component, OnInit } from '@angular/core';
import { CustomServiceService } from '../../ez-services/custom-service.service';
import { HttpErrorResponse } from "@angular/common/http";
import * as $ from 'jquery';

@Component({
  selector: 'app-rate-mapping',
  templateUrl: './rate-mapping.component.html',
  styleUrls: ['./rate-mapping.component.css']
})
export class RateMappingComponent implements OnInit {


  dtRateMapingDetails: any = [];
  objratemaping: any = {};
  id: any;
  isEdit: boolean = false;
  btnTextsave: any;
  btnTextclear: any;
  httpLoader: boolean = false;
  txtRatemappingSearch:any;
  PageNo:any;

  constructor(public _service: CustomServiceService) { }

  // Page Intiation
  ngOnInit() {
    this.GetRateMappingData();
    this.btnTextsave = "Save";
    this.btnTextclear = "Clear";
  }

  // Getting RateMapping Details
  GetRateMappingData() {
    this.httpLoader = true;
    this._service.getmethod('api/RateMapping').subscribe(res => {
      this.dtRateMapingDetails = res;
      // this.dtRateMapingDetails.unshift(res);
      //console.log(this.dtRateMapingDetails);
    }, (err: HttpErrorResponse) => {

      if (err.error instanceof Error) {
        console.log("client side error");
      }
      else {
        console.log("Server side error");
      }
      this.httpLoader = false;
    })
  }

  // Click Change Event Method
  SaveRateMapping() {
    if (this.isEdit == true) {
      this.Updatingratemapping();
    }
    else {
      this.InsertRateMapping();
    }
  }


  // Save RateMapping
  InsertRateMapping() {

    if (!this.objratemaping.txtKilometer) {
      alert("Please Enter Kilometer");
      $("#txtKilometer").focus();
    }
    else if (!this.objratemaping.txtkmamt) {
      alert("Please Enter Kilometer Amount");
      $("#txtkmamt").focus();
    }
    else if (!this.objratemaping.txtWeightgrms) {
      alert("Please Enter Weight");
      $("#txtWeightgrms").focus();
    }
    else if (!this.objratemaping.txtweightamt) {
      alert("Please Enter Weight Amount");
      $("#txtweightamt").focus();
    }
    else {
      this.httpLoader = true;

      let objratemappingparam: any = {};
      // console.log(this.objratemaping);

      objratemappingparam.KM = this.objratemaping.txtKilometer;
      objratemappingparam.KMAmount = this.objratemaping.txtkmamt;
      objratemappingparam.Weight_gms = this.objratemaping.txtWeightgrms;
      objratemappingparam.WeightAmount = this.objratemaping.txtweightamt;

      this._service.PostMethodWithHeaders(objratemappingparam, 'api/RateMapping').subscribe(res => {
        //console.log(res);
        this.GetRateMappingData();
        this.objratemaping = {};
        alert("RateMapping Created ");
        this.httpLoader = false;

      }, (err: HttpErrorResponse) => {

        if (err.error instanceof Error) {
          console.log("client side error");
        }
        else {
          console.log("Server side error");
        }
        this.httpLoader = false;
      })

    }
  }

  // Edit the values
  EditRateMapping(row) {
    this.objratemaping.txtKilometer = row.KM;
    this.objratemaping.txtkmamt = row.KMAmount;
    this.objratemaping.txtWeightgrms = row.Weight_gms;
    this.objratemaping.txtweightamt = row.WeightAmount;
    this.id = row.id;
    this.isEdit = true;
    this.btnTextsave = "Update";
    this.btnTextclear = "Cancel";
  }


  // Updating the values

  Updatingratemapping() {

    if (!this.objratemaping.txtKilometer) {
      alert("Please Enter Kilometer");
      $("#txtKilometer").focus();
    }
    else if (!this.objratemaping.txtkmamt) {
      alert("Please Enter Kilometer Amount");
      $("#txtkmamt").focus();
    }
    else if (!this.objratemaping.txtWeightgrms) {
      alert("Please Enter Weight");
      $("#txtWeightgrms").focus();
    }
    else if (!this.objratemaping.txtweightamt) {
      alert("Please Enter Weight Amount");
      $("#txtweightamt").focus();
    }
    else {

      this.httpLoader = true;
      let objratemappingparam: any = {};
      objratemappingparam.KM = this.objratemaping.txtKilometer;
      objratemappingparam.KMAmount = this.objratemaping.txtkmamt;
      objratemappingparam.Weight_gms = this.objratemaping.txtWeightgrms;
      objratemappingparam.WeightAmount = this.objratemaping.txtweightamt;
      objratemappingparam.id = this.id;

      this._service.PostMethodWithHeaders(objratemappingparam, 'api/RateMappingPut').subscribe(res => {
        //console.log(res);
        this.GetRateMappingData();
        this.objratemaping = {};
        alert("RateMapping Updated ");
        this.httpLoader = false;
      }, (err: HttpErrorResponse) => {

        if (err.error instanceof Error) {
          console.log("client side error");
        }
        else {
          console.log("Server side error");
        }
        this.httpLoader = false;
      })
    }
  }


  // Claearing the feilds
  ClearRateMapping() {
    this.objratemaping = {};
    this.btnTextsave = "Save";
    this.btnTextclear = "Clear";
  }



}
