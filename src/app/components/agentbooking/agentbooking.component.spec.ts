import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentbookingComponent } from './agentbooking.component';

describe('AgentbookingComponent', () => {
  let component: AgentbookingComponent;
  let fixture: ComponentFixture<AgentbookingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgentbookingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentbookingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
