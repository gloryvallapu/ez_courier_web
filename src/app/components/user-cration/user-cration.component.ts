import { Component, OnInit } from '@angular/core';
import { CustomServiceService } from '../../ez-services/custom-service.service';
import { HttpErrorResponse } from "@angular/common/http";
import * as $ from 'jquery';
import * as crypto from "crypto-js";
// import { Router } from '@angular/router';

@Component({
  selector: 'app-user-cration',
  templateUrl: './user-cration.component.html',
  styleUrls: ['./user-cration.component.css']
})
export class UserCrationComponent implements OnInit {

  //declare the variables...
  objReg: any = {};
  regHide: boolean = false;
  Password: string = "";
  httpLoader: boolean = false;

  //inherit the custom service...
  constructor(public _serv: CustomServiceService) { }

  ngOnInit() {

  }

  // Sending OTP as SMS After Registration
  public CheckUserExistance(): void {

    if (!this.objReg.txtFullName) {
      alert("Please enter FullName");
      $("#txtFullName").focus();
    }

    else if (!this.objReg.txtMobileNo) {
      alert("please enter MobileNo");
      $("#txtMobileNo").focus();
    }

    else if (!this.objReg.txtEmail) {
      alert("please enter EmailId");
      $("#txtEmail").focus();
    }
    else {

      this.httpLoader = true;

      let objValidateUser: any = {};
      objValidateUser.MobileNo = this.objReg.txtMobileNo;
      objValidateUser.EmailId = this.objReg.txtEmail;

      // checking user existance
      this._serv.PostMethodWithHeaders(objValidateUser, 'api/ValidateUser').subscribe(res => {
        if (res.length > 0) {
          if (res[0].status == 4) { // not existing user details

            //  sending the OTP to non existing customer
            this.SendOTP(this.objReg.txtMobileNo);

          } else {
            alert(res[0].statusMessage); // existing mobile/email/loginID
          }
        }

        this.httpLoader = false;

      }, (err: HttpErrorResponse) => {

        if (err.error instanceof Error) {
          console.log("client side error");
        }
        else {
          console.log("Server side error");
        }
        this.httpLoader = false;
      })


    }
  }

  public fnOTP1KeyPress() {
    $("#txtOTP2").focus();
  }

  public fnOTP2KeyPress() {
    $("#txtOTP3").focus();
  }
  public fnOTP3KeyPress() {
    $("#txtOTP4").focus();
  }
  public fnOTP4KeyPress() {
    $("#txtOTP5").focus();
  }
  // public fnOTP5KeyPress(){
  //   $("#txtOTP2").focus();
  // }

  public SendOTP(mobile): void {
    this.Password = this._serv.fnGetUniqueNo();
    let Message = "Use : " + this.Password + " as OTP. never share your OTP.";
    let objsms = { 'mobileNo': mobile, 'message': Message };
    // console.log(this.Password);
    this._serv.PostMethodWithHeaders(objsms, "api/SendSMS").subscribe(response => {

      alert("OTP is sent to your registered mobile");
      this.regHide = true;
      $("#txtOTP").focus();

    }, err => {
      this.regHide = false;

    })
  }


  // saving user details
  public SaveUser(): void {
    var OTP: any = "";
    if (this.objReg.txtOTP1 && this.objReg.txtOTP2 && this.objReg.txtOTP3 && this.objReg.txtOTP4 && this.objReg.txtOTP5) {
     
      OTP = this.objReg.txtOTP1.toString() + this.objReg.txtOTP2.toString() + this.objReg.txtOTP3.toString() + this.objReg.txtOTP4.toString() + this.objReg.txtOTP5.toString();
      // console.log(OTP);
    }


    if (!this.objReg.txtOTP1 || !this.objReg.txtOTP2 || !this.objReg.txtOTP3 || !this.objReg.txtOTP4 || !this.objReg.txtOTP5) {
      alert("Please enter OTP");
      $("#txtOTP1").focus();
    }
    else if (OTP != this.Password) {
      alert("OTP didn't match");
      $("#txtOTP1").focus();

    }
    else if (!this.objReg.txtPassword) {
      alert("please enter Password");
      $("#txtPassword").focus();
    }
    else if (!this.objReg.txtconfirmPassword) {
      alert("Enter Confirm Password");
      $("#txtconfirmPassword").focus();
      $("#txtPassword").focus();
    }
    else if (this.objReg.txtPassword != this.objReg.txtconfirmPassword) {
      alert("Password and confirm password didn't match");
      $("#txtconfirmPassword").focus();

    }
    else {

      this.httpLoader = true;

      let objUserParam: any = {};

      let encpwd = crypto.DES.encrypt(this.objReg.txtPassword, this._serv.key).toString();

      objUserParam.FullName = this.objReg.txtFullName;
      objUserParam.MobileNo = this.objReg.txtMobileNo;
      objUserParam.EmailId = this.objReg.txtEmail;
      objUserParam.UserPwd = encpwd;

      this._serv.PostMethodWithHeaders(objUserParam, 'api/UserReg').subscribe(res => {
        alert("Registered successfully");
        this.regHide = false;
        //console.log(res);
        this.objReg = {};
        this.httpLoader = false;
      }, (err: HttpErrorResponse) => {

        if (err.error instanceof Error) {
          console.log("client side error");
        }
        else {
          console.log("Server side error");
        }
        this.httpLoader = false;
      })
    }
  }

}