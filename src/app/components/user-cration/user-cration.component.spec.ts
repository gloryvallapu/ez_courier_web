import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserCrationComponent } from './user-cration.component';

describe('UserCrationComponent', () => {
  let component: UserCrationComponent;
  let fixture: ComponentFixture<UserCrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserCrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserCrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
