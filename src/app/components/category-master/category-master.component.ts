import { Component, OnInit } from '@angular/core';
import { CustomServiceService } from '../../ez-services/custom-service.service';
import { HttpErrorResponse } from "@angular/common/http";
import * as $ from 'jquery';

@Component({
  selector: 'app-category-master',
  templateUrl: './category-master.component.html',
  styleUrls: ['./category-master.component.css']
})
export class CategoryMasterComponent implements OnInit {

  objCategory: any = {};
  dtCategory: any = [];
  url: string = "";
  pUrl: any;
  itemId: any;
  putUrl: any;
  delUrl: any;
  pageNo:any;

  btnTextsave: any;
  btnTextclear: any;
  // displayBtn: boolean = true;
  // displaySucess: boolean = false;
  httpLoader: boolean = false;
  ItemSearch: any;


  // Call the Customer Service(for get put post delete methods purpose...)
  constructor(private _serv: CustomServiceService) { }

  ngOnInit() {
    //Bind the page load data here
    this.GetCategory();
    this.btnTextsave = "Save";
    this.btnTextclear = "Clear";
  }



  /* get all categories */
  private GetCategory(): void {

    this.httpLoader = true;

    this.url = "api/CategoryInsert";
    this._serv.getmethod(this.url).subscribe(res => {
      this.dtCategory = res;
      this.httpLoader = false;
      // console.log(this.dtCategory);
    },
      //Errors Purpose....
      (err: HttpErrorResponse) => {

        if (err.error instanceof Error) {
          console.log("client side error");
        }
        else {
          console.log("Server side error");
        }
        this.httpLoader = false;
      })

  }
  resetObj() {
    this.objCategory = {}
    this.btnTextsave = "Save";
    this.btnTextclear = "Clear";
  }
  /* insert the data */

  SaveItems() {

    if (!this.objCategory.txtCategory) {
      alert("please enter Category");
      $("#txtCategory").focus();
    }
    else {
      this.httpLoader = true;

      this.pUrl = "api/CategoryInsert";
      let body: any = {
        "CategoryName": this.objCategory.txtCategory,
        "Description": this.objCategory.txtDesc,
        "IsActive": this.objCategory.chkkBox
      };
      this._serv.PostMethodWithHeaders(body, this.pUrl).subscribe(data => {
        this.GetCategory();
        this.resetObj();
        alert("Category created");
        this.httpLoader = false;

      },
        //Errors Purpose....
        (err: HttpErrorResponse) => {

          if (err.error instanceof Error) {
            console.log("client side error");
          }
          else {
            console.log("Server side error");
          }
          this.httpLoader = false;
        })
    }
    // this.displaySucess=true;
  }

  /* Bind The Data at the time of edit */
  editCat(item) {
    // this.displayBtn = false;
    this.objCategory.txtCategory = item.CategoryName;
    this.objCategory.txtDesc = item.Description;
    this.objCategory.chkkBox = item.IsActive;
    console.log(this.objCategory.chkkBox)
    this.itemId = item.CategoryID
    this.btnTextsave = "Update";
    this.btnTextclear = "Cancel";
  }

  /* Update the Data */
  editItems() {

    this.httpLoader = true;

    this.putUrl = "api/CategoryPut";
    let body: any = {
      "CategoryName": this.objCategory.txtCategory,
      "Description": this.objCategory.txtDesc,
      "IsActive": this.objCategory.chkkBox,
      "CategoryID": this.itemId
    }
    this._serv.PostMethodWithHeaders(body, this.putUrl).subscribe(data => {
      //console.log(data, body);
      this.GetCategory();
      // this.displayBtn = true;
      this.resetObj();
      alert("Category Updated");
      this.httpLoader = false;
    },
      //Errors Purpose....
      (err: HttpErrorResponse) => {

        if (err.error instanceof Error) {
          console.log("client side error");
        }
        else {
          console.log("Server side error");
        }
        this.httpLoader = false;
      })

  }

  /* Delete the Data */
  delObj(item) {
    //console.log(item.CategoryID);
    this.httpLoader = true;
    this.delUrl = "api/CategoryPut";
    this._serv.getwithId(this.delUrl, item.CategoryID).subscribe(data => {

      //console.log(data);
      this.GetCategory();
      alert("Category InActive");
      this.httpLoader = false;

    });
  }
}
