import { Component, OnInit } from '@angular/core';
import { CustomServiceService } from '../../ez-services/custom-service.service';
import { FormControl, Validators, FormBuilder } from '@angular/forms';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerobj: any = {};
  RegisterFormObj: any = {};
  constructor(private service: CustomServiceService, private fb: FormBuilder) { }

  ngOnInit() {
    //get all data
    this.onloadData();
  }

  //function for get onloaddata.
  onloadData() {
    let Url = "api/AgentReg";
    this.service.getmethod(Url).subscribe(response => {
      this.registerobj.allAgentsData = response;
      this.registerobj.registerFormAndEditButtonShow = 0;
    }, err => {

    })
  }
  //function for select Aadhar Doc.
  upLoadAadharDoc(fileInput) {
    if (fileInput.target.files && fileInput.target.files.length > 0) {
      this.registerobj.AadharDoc = fileInput.target.files[0];
      // var file = IO.newFile("Home", "myfile.txt");
      // this.registerobj.AadharDoc.Move("D:/My Projects/src/assets/UploadFiles")

    }

  }
  //function for Registeration form Submit.
  RegisterationSubmit(obj) {

    let url = "api/FileUpload";
    this.service.uploadFiles(this.registerobj.AadharDoc, url).subscribe(response => {
      let rquestObj: any = {};
      rquestObj.AgentName = obj.AgentName;
      rquestObj.PersonalMobileNo = obj.PersonalMobileNo;
      rquestObj.OfficialMobileNo = obj.OfficialMobileNo;
      rquestObj.Address1 = obj.Address1;
      rquestObj.Address2 = obj.Address2;
      rquestObj.Address3 = obj.Address3;
      rquestObj.VehicleName = obj.VehicleName;
      rquestObj.VehicleNo = obj.VehicleNo;
      rquestObj.AadharNo = obj.AadharNo;
      rquestObj.PanNo = obj.PanNo;
      rquestObj.PaymentMode = obj.PaymentMode;
      rquestObj.DrivingLNo = obj.DrivingLNo;
      rquestObj.AadharDoc = "http://ez-korier.com/images/uploadfiles/"+response.FileName;
      rquestObj.BankAccountNo = obj.BankAccountNo;
      rquestObj.BankName = obj.BankName;
      rquestObj.Branch = obj.Branch;
      rquestObj.IFSC = obj.IFSC;
      rquestObj.Description = obj.Description;
      if (obj.IsActive === undefined || obj.IsActive === false) {
        rquestObj.IsActive = false;
      } else {
        rquestObj.IsActive = true;
      }

      let Url = "api/AgentReg";
      this.service.PostMethodWithHeaders(rquestObj, Url).subscribe(response => {
        this.RegisterFormObj = {};
        this.onloadData();

      }, err => {

      })
    }, err => {
    })






  }

  //function for edit Registeration form Submit.
  editRegisterationSubmit(obj) {
    let url = "api/AgentRegPUT";
    this.service.PostMethodWithHeaders(obj, url).subscribe(response => {
      this.onloadData();
      this.RegisterFormObj = {};
    }, err => {

    })
  }


  //function for deleteAgent.
  deleteAgent(slectedAgent) {
    let Url = "api/AgentRegPUT";
    this.service.getwithId(Url, slectedAgent.AgentID).subscribe(response => {
      this.onloadData();
      this.RegisterFormObj = {};

    }, err => {

    })

  }

  //function for Cancel Registration Form.
  CancelRegistrationForm() {
    this.registerobj.registerFormAndEditButtonShow = 0;
    this.RegisterFormObj = {};

  }

  //function for edit Agent.
  EditAgent(slectedAgent) {
    delete this.registerobj.registerFormAndEditButtonShow;
    this.RegisterFormObj = {};
    this.RegisterFormObj = slectedAgent;

  }

}
