import { Component, OnInit } from '@angular/core';
import { Headers, RequestOptions, Http } from '@angular/http';
import { CustomServiceService } from '../../ez-services/custom-service.service';
import { FormControl, Validators, FormBuilder } from '@angular/forms';
import * as crypto from "crypto-js";
import { HttpErrorResponse } from "@angular/common/http";
import * as $ from 'jquery';

import { async } from 'q';


@Component({
  selector: 'app-agent-creation',
  templateUrl: './agent-creation.component.html',
  styleUrls: ['./agent-creation.component.css']
})
export class AgentCreationComponent implements OnInit {

  registerobj: any = {};
  RegisterFormObj: any = {};
  DocumentsObj: any = {};
  Password: string = "";
  httpLoader: boolean = false;
  isReadonly: boolean = false;
  txtSearch: any;
  pageNo: any;
  latitude: any;
  longitude: any;
  location: string;
  isEdit: boolean = false;
  imageAadharSrc: string;
  imageDLSrc: string;
  imageProfileSrc: string;
  agentID: any;

  constructor(public service: CustomServiceService, private fb: FormBuilder, private http: Http) { }

  ngOnInit() {
    //get all data
    this.getCurrentLocation();
    this.GetAgents();
    this.Password = this.service.fnGetUniqueNo();

    this.RegisterFormObj.PaymentMode = 0;
  }


  getCurrentLocation(): void {


    navigator.geolocation.getCurrentPosition((position) => {

      this.latitude = position.coords.latitude;
      this.longitude = position.coords.longitude;

      //console.log(latitude, longitude)

      let geocoder = new google.maps.Geocoder;
      let latlng = { lat: this.latitude, lng: this.longitude };
      geocoder.geocode({ 'location': latlng }, (results, status) => {
        this.location = results[0].formatted_address;
        // console.log(results[0].formatted_address); // read data from here
        // console.log(status);
      });

    });
  }

  //function for get onloaddata.
  GetAgents() {
    this.httpLoader = true;
    let Url = "api/AgentReg";
    this.service.getmethod(Url).subscribe(response => {
      //console.log(JSON.stringify(response));

      if (response.length > 0) {
        this.registerobj.allAgentsData = response;
      }
      else {
        this.registerobj.allAgentsData = [];
      }

      this.registerobj.registerFormAndEditButtonShow = 0;
      this.httpLoader = false;
    }, err => {
      this.httpLoader = false;
    })
  }

  // getting aadhar file 
  AadharFileChange(event) {   //upLoadAadharDoc

    if (event.target.files && event.target.files[0]) {

      this.DocumentsObj.AadharDoc = event.target.files[0];

      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.imageAadharSrc = event.target.result;
      }
      reader.readAsDataURL(event.target.files[0]);
    }


  }

  // getting Driving licence file
  DLFileChange(event) {

    if (event.target.files && event.target.files[0]) {

      this.DocumentsObj.DLDoc = event.target.files[0];

      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.imageDLSrc = event.target.result;
      }
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  // getting Profile pic file
  ProfileFileChange(event) { //UploadProfilePic

    if (event.target.files && event.target.files[0]) {

      this.DocumentsObj.ProfilePic = event.target.files[0];

      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.imageProfileSrc = event.target.result;
      }
      reader.readAsDataURL(event.target.files[0]);
    }
  }


  //function for Registeration form Submit.
  RegisterationSubmit(obj) {

    if (!obj.AgentName) {
      alert("please enter agent name");
      $("#txtAgentName").focus();
    }
    else if (!obj.PersonalMobileNo) {
      alert("please enter mobile number");
      $("#txtPersonalMobileNo").focus();
    }
    else if (!obj.OfficialMobileNo) {
      alert("please enter official mobile number");
      $("#txtOfficialMobileNo").focus();
    }
    else if (!obj.Address1) {
      alert("please enter Address");
      $("#txtAddress1").focus();
    }
    else if (!obj.BankAccountNo) {
      alert("please enter BankAccount no.");
      $("#txtBankAccountNo").focus();
    }
    else if (!obj.VehicleName) {
      alert("please enter vehicle name");
      $("#txtVechileName").focus();
    }
    else if (!obj.VehicleNo) {
      alert("please enter Vechile number");
      $("#txtVechileNo").focus();
    }
    else if (!obj.AadharNo) {
      alert("please enter Aadhar Number");
      $("#txtAadharNo").focus();
    }
    else if (!obj.PanNo) {
      alert("please enter Pan number");
      $("#txtPanNo").focus();
    }
    else if (!obj.DrivingLNo) {
      alert("please enter Driving Licence Number");
      $("#txtDrivingLNo").focus();
    }
    else if (!obj.PaymentMode) {
      alert("please select Payment Mode");
      $("#ddlPaymentMode").focus();
    }
    else if (!obj.BankName) {
      alert("please enter Bank name");
      $("#txtBankName").focus();
    }
    else if (!obj.Branch) {
      alert("please enter Branch");
      $("#txtBranch").focus();
    }
    else if (!obj.IFSC) {
      alert("please enter IFSC code");
      $("#txtIFSC").focus();
    }
    else if (!obj.EmailID) {
      alert("please enter Email ID");
      $("#txtEmailId").focus();
    }
    else if (!this.service.fnValidateEmailAddress(obj.EmailID)) {
      alert("please enter correct Email ID");
      $("#txtEmailId").focus();
    }
    else if (!this.DocumentsObj.AadharFileName) {
      alert("please Upload Aadhar document");
      $("#Aadhardocfile").focus();
    }
    else if (!this.DocumentsObj.DLFileName) {
      alert("please Upload Driving Licence document");
      $("#DldocId").focus();
    }
    else if (!this.DocumentsObj.ProfileFileName) {
      alert("please Upload profile photo");
      $("#ProfilePic").focus();
    }
    else {
      this.httpLoader = true;

      let objValidateUser: any = {};
      objValidateUser.MobileNo = obj.PersonalMobileNo;
      objValidateUser.EmailId = obj.EmailID;

      this.service.PostMethodWithHeaders(objValidateUser, 'api/ValidateUser').subscribe(res => {
        if (res.length > 0) {

          // Not existing mail,mobile,login id
          if (res[0].status.toString() == "4") {

            this.SaveAgent(obj);

            // this.UploadAadharfile(obj);
          }
          else {

            alert(res[0].statusMessage);
            this.httpLoader = false;
          }
        }

      }, (err: HttpErrorResponse) => {

        if (err.error instanceof Error) {
          console.log("client side error");
        }
        else {
          console.log("Server side error");
        }
        this.httpLoader = false;
      })
    }
  }



  // uploading Aadhar card
  UploadAadharfile() {

    if (!this.DocumentsObj.AadharDoc) {
      alert("please choose Aadhar document");
      $("#Aadhardocfile").focus();
    }
    else {

      this.service.uploadFiles(this.DocumentsObj.AadharDoc, "api/FileUpload").subscribe(Response => {
        this.DocumentsObj.AadharFileName = "http://ez-korier.com/images/uploadfiles/" + Response.FileName;
        console.log("AAdhar : " + JSON.stringify(Response));
      }, err => {

      })
    }
  }

  // uploading driving licence
  UploadDLfile() {

    if (!this.DocumentsObj.DLDoc) {
      alert("please choose Driving Licence document");
      $("#DldocId").focus();
    }
    else {

      this.service.uploadFiles(this.DocumentsObj.DLDoc, "api/FileUpload").subscribe(Response => {

        this.DocumentsObj.DLFileName = "http://ez-korier.com/images/uploadfiles/" + Response.FileName;
        console.log("DL : " + JSON.stringify(Response));

      }, err => {

      })
    }

  }

  // uploading profile pic
  UploadProfilePicfile() {

    if (!this.DocumentsObj.ProfilePic) {
      alert("please choose profile photo");
      $("#ProfilePic").focus();
    }
    else {
      this.service.uploadFiles(this.DocumentsObj.ProfilePic, "api/FileUpload").subscribe(Response => {

        this.DocumentsObj.ProfileFileName = "http://ez-korier.com/images/uploadfiles/" + Response.FileName;
        console.log("PROFILE  : " + JSON.stringify(Response));

      }, err => {

      })
    }
  }


  // Save Agent Details
  SaveAgent(obj) {

    this.httpLoader = true;

    // this.RegisterFormObj.AadharDoc = this.DocumentsObj.AadharFileName;
    // this.RegisterFormObj.DrivingLCDoc = this.DocumentsObj.DLFileName;
    // this.RegisterFormObj.ProfilePic = this.DocumentsObj.ProfileFileName;

    let rquestObj: any = {};

    rquestObj.AgentName = obj.AgentName;
    rquestObj.PersonalMobileNo = obj.PersonalMobileNo;
    rquestObj.OfficialMobileNo = obj.OfficialMobileNo;
    rquestObj.Address1 = obj.Address1;
    rquestObj.Address2 = obj.Address2;
    rquestObj.Address3 = obj.Address3;
    rquestObj.VehicleName = obj.VehicleName;
    rquestObj.VehicleNo = obj.VehicleNo;
    rquestObj.AadharNo = obj.AadharNo;
    rquestObj.PanNo = obj.PanNo;
    rquestObj.PaymentMode = obj.PaymentMode;
    rquestObj.DrivingLNo = obj.DrivingLNo;

    rquestObj.AadharDoc = this.DocumentsObj.AadharFileName;
    rquestObj.DLcopy = this.DocumentsObj.DLFileName;
    rquestObj.ProfilePic = this.DocumentsObj.ProfileFileName;

    rquestObj.BankAccountNo = obj.BankAccountNo;
    rquestObj.BankName = obj.BankName;
    rquestObj.Branch = obj.Branch;
    rquestObj.IFSC = obj.IFSC;
    rquestObj.Description = obj.Description;
    rquestObj.agentComission_KM = obj.comission;

    // encrypting the generated password
    let encpwd = crypto.DES.encrypt(this.Password, this.service.key).toString();
    rquestObj.UserPwd = encpwd;

    rquestObj.FullName = obj.AgentName;
    rquestObj.EmailID = obj.EmailID;

    if (obj.IsActive === undefined || obj.IsActive === false) {
      rquestObj.IsActive = false;
    } else {
      rquestObj.IsActive = true;
    }

    rquestObj.Latitude = this.latitude;
    rquestObj.longitude = this.longitude;
    rquestObj.IsOnline = false;
    rquestObj.location = this.location;


    let Url = "api/AgentReg";
    this.service.PostMethodWithHeaders(rquestObj, Url).subscribe(response => {
      this.RegisterFormObj = {};
      this.SendSMS(obj);
      this.GetAgents();
      // this.onloadData();
      this.httpLoader = false;
      this.httpLoader = false;
    }, err => {
      this.httpLoader = false;
    })


  }


  // Sending password as SMS After Registration
  SendSMS(obj) {
    let Message = "Use Password as : " + this.Password + "";
    let objsms = { 'mobileNo': obj.PersonalMobileNo, 'message': Message };

    this.service.PostMethodWithHeaders(objsms, "api/SendSMS").subscribe(response => {
      // this.GetAgents();
    }, err => {

    })
  }


  //update the agent details
  UpdateAgent(obj) {



    if (!obj.AgentName) {
      alert("please enter agent name");
      $("#txtAgentName").focus();
    }
    else if (!obj.PersonalMobileNo) {
      alert("please enter mobile number");
      $("#txtPersonalMobileNo").focus();
    }
    else if (!obj.OfficialMobileNo) {
      alert("please enter official mobile number");
      $("#txtOfficialMobileNo").focus();
    }
    else if (!obj.Address1) {
      alert("please enter Address");
      $("#txtAddress1").focus();
    }
    else if (!obj.BankAccountNo) {
      alert("please enter BankAccount no.");
      $("#txtBankAccountNo").focus();
    }
    else if (!obj.VehicleName) {
      alert("please enter vehicle name");
      $("#txtVechileName").focus();
    }
    else if (!obj.VehicleNo) {
      alert("please enter Vechile number");
      $("#txtVechileNo").focus();
    }
    else if (!obj.AadharNo) {
      alert("please enter Aadhar Number");
      $("#txtAadharNo").focus();
    }
    else if (!obj.PanNo) {
      alert("please enter Pan number");
      $("#txtPanNo").focus();
    }
    else if (!obj.DrivingLNo) {
      alert("please enter Driving Licence Number");
      $("#txtDrivingLNo").focus();
    }
    else if (!obj.PaymentMode) {
      alert("please select Payment Mode");
      $("#ddlPaymentMode").focus();
    }
    else if (!obj.BankName) {
      alert("please enter Bank name");
      $("#txtBankName").focus();
    }
    else if (!obj.Branch) {
      alert("please enter Branch");
      $("#txtBranch").focus();
    }
    else if (!obj.IFSC) {
      alert("please enter IFSC code");
      $("#txtIFSC").focus();
    }
    else if (!obj.EmailID) {
      alert("please enter Email ID");
      $("#txtEmailId").focus();
    }
    else if (!this.service.fnValidateEmailAddress(obj.EmailID)) {
      alert("please enter correct Email ID");
      $("#txtEmailId").focus();
    }
    else {

      console.log(JSON.stringify(obj));
      this.httpLoader = true;
      let rquestObj: any = {};

      rquestObj.AgentName = obj.AgentName;
      rquestObj.PersonalMobileNo = obj.PersonalMobileNo;
      rquestObj.OfficialMobileNo = obj.OfficialMobileNo;
      rquestObj.Address1 = obj.Address1;
      rquestObj.Address2 = obj.Address2;
      rquestObj.Address3 = obj.Address3;
      rquestObj.VehicleName = obj.VehicleName;
      rquestObj.VehicleNo = obj.VehicleNo;
      rquestObj.AadharNo = obj.AadharNo;
      rquestObj.PanNo = obj.PanNo;
      rquestObj.PaymentMode = obj.PaymentMode;
      rquestObj.DrivingLNo = obj.DrivingLNo;

      rquestObj.AadharDoc = this.DocumentsObj.AadharFileName;
      rquestObj.DrivingLCDoc = this.DocumentsObj.DLFileName;
      rquestObj.ProfilePic = this.DocumentsObj.ProfileFileName;

      rquestObj.BankAccountNo = obj.BankAccountNo;
      rquestObj.BankName = obj.BankName;
      rquestObj.Branch = obj.Branch;
      rquestObj.IFSC = obj.IFSC;
      rquestObj.Description = obj.Description;
      rquestObj.Commission_KM = obj.comission;
      rquestObj.AgentID = this.agentID;

      rquestObj.EmailID = obj.EmailID;

      if (obj.IsActive === undefined || obj.IsActive === false) {
        rquestObj.IsActive = false;
      } else {
        rquestObj.IsActive = true;
      }


      let url = "api/AgentRegPUT";
      this.service.PostMethodWithHeaders(rquestObj, url).subscribe(response => {

        this.GetAgents();
        this.CancelRegistrationForm();

      }, err => {
        this.httpLoader = false;
      })

    }
  }

  //function for deleteAgent.
  deleteAgent(slectedAgent) {

    this.httpLoader = true;

    let Url = "api/AgentRegPUT";
    this.service.getwithId(Url, slectedAgent.AgentID).subscribe(response => {
      this.GetAgents();
      this.RegisterFormObj = {};
      this.httpLoader = false;
    }, err => {
      this.httpLoader = false;
    })

  }

  //function for Cancel Registration Form.
  CancelRegistrationForm() {
    this.registerobj.registerFormAndEditButtonShow = 0;
    this.RegisterFormObj = {};
    this.isReadonly = false;
    this.DocumentsObj = {};
    this.Password = "";
    this.httpLoader = false;
    this.txtSearch = null;
    this.isEdit = false;
    this.imageAadharSrc = null;
    this.imageDLSrc = null;
    this.imageProfileSrc = null;
    this.pageNo = 1;

  }

  //function for edit Agent.
  EditAgent(selctedAgent) {

    console.log(JSON.stringify(selctedAgent));
    this.httpLoader = true;
    delete this.registerobj.registerFormAndEditButtonShow;
    this.RegisterFormObj = {};
    this.RegisterFormObj = selctedAgent;
    this.RegisterFormObj.EmailID = selctedAgent.EmailId;
    this.RegisterFormObj.comission = selctedAgent.Commission_KM;

    this.imageAadharSrc = this.DocumentsObj.AadharFileName = selctedAgent.AadharDoc;
    this.imageDLSrc = this.DocumentsObj.DLFileName = selctedAgent.DrivingLCDoc;
    this.imageProfileSrc = this.DocumentsObj.ProfileFileName = selctedAgent.ProfilePic;
    this.agentID = selctedAgent.AgentID;
    this.isEdit = true;
    this.isReadonly = true;
    this.httpLoader = false;





  }

}
