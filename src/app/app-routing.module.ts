import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserbookingComponent } from './components/userbooking/userbooking.component';
import { AgentbookingComponent } from './components/agentbooking/agentbooking.component';
import { AuthGuard } from './auth/auth.guard';
import { RegisterComponent } from './components/register/register.component';
import { ItemMasterComponent } from './components/item-master/item-master.component'
import { RateMappingComponent } from './components/rate-mapping/rate-mapping.component';
import { CategoryMasterComponent } from './Components/category-master/category-master.component';
import { LoginComponent } from './components/login/login.component';
import { AdminDashboardComponent } from './components/admin-dashboard/admin-dashboard.component';
import { AgentCreationComponent } from './components/agent-creation/agent-creation.component';
import { AgentWiseReportComponent } from './components/agent-wise-report/agent-wise-report.component';
import { AgentPaymentsComponent } from './components/agent-payments/agent-payments.component';
import { HomeComponent } from './components/home/home.component';
import { PaymentRegisterComponent } from './components/payment-register/payment-register.component';
import { UserbookingRegisterComponent } from './Components/userbooking-register/userbooking-register.component';
// import { CareersComponent } from './components/careers/careers.component';
import { AgentRequestsComponent } from './components/agent-requests/agent-requests.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { RefundOrdersComponent } from './components/refund-orders/refund-orders.component';

const routes: Routes = [

  { path: '', component: HomeComponent, pathMatch: 'full' },
  { path: 'UserBooking', component: UserbookingComponent },
  { path: 'Register', component: RegisterComponent },
  { path: 'Agent', component: AgentCreationComponent },
  { path: 'login', component: LoginComponent },
  { path: 'Items', component: ItemMasterComponent },
  { path: 'RateMapping', component: RateMappingComponent },
  { path: 'Category', component: CategoryMasterComponent },
  { path: 'home', component: AgentbookingComponent, canActivate: [AuthGuard] },
  { path: 'AdminDashboard', component: AdminDashboardComponent },
  { path: 'AgentWiseRegister', component: AgentWiseReportComponent },
  { path: 'AgentPayments', component: AgentPaymentsComponent },
  { path: 'PaymentRegister', component: PaymentRegisterComponent },
  { path: 'UserbookingRegister', component: UserbookingRegisterComponent },
  // { path: 'Careers', component: CareersComponent},
  { path: 'AgentRequests', component: AgentRequestsComponent },
  { path: 'ChangePwd', component: ChangePasswordComponent },
  { path: 'RefundOrders', component: RefundOrdersComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
