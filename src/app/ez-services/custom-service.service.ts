import { Injectable } from '@angular/core';
import { Headers, RequestOptions, Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs';
import 'rxjs/add/operator/map';
// import { Observable } from "rxjs/Rx";
import { Observable } from 'rxjs/Observable';
import * as io from 'socket.io-client';
import { Router } from '@angular/router';
import * as crypto from "crypto-js";

@Injectable()
export class CustomServiceService {
  socket: any;
  datas: any;
  path = "https://ez-kourier.com/";
  key = "S8W8A8M5I1R8O7Y4N8A8NI";
  loggedIn = new BehaviorSubject<boolean>(false); // {1}

  options: any;


  get isLoggedIn() {
    return this.loggedIn.asObservable(); // {2}
  }


  constructor(public http: Http, private router: Router) {

    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('SecureCode', 'S8W8A8M5I1R8O7Y4N8A8NI');
    this.options = new RequestOptions({ headers: headers });



    //this.socket = io(this.path);
    if (localStorage.getItem("UserDetails") !== null) {

      this.loggedIn.next(true);
    }
  }

  login(obj, url) {

    // let body = JSON.stringify(obj);

    return this.http.post(this.path + url, obj, this.options).map(res => res.json()).subscribe(data => {
      // console.log(data);
      if (data.length > 0) {

        let encPwd = data[0].UserPwd;
        let decPwd = crypto.DES.decrypt(encPwd, this.key).toString(crypto.enc.Utf8);
        let enterdPwd = obj.UserPwd;
        if (enterdPwd == decPwd) {
          this.loggedIn.next(true);

          localStorage.setItem("UserDetails", JSON.stringify(data[0]));

          // this.router.navigate(['/AdminDashboard']);
          this.router.navigateByUrl('/AdminDashboard');
        }
        else {
          this.loggedIn.next(false);
          alert("Invalid password");
        }
      }
      else {
        this.loggedIn.next(false);
        alert("Invalid LoginID");
      }
    })

  }

  getmethod(url) {

    return this.http.get(this.path + url, this.options).map(res => res.json())
      .catch(err => this.handleError(err));
  }

  //get methode.
  getwithId(url, id) {
    return this.http.get(this.path + url + "/" + id, this.options).map(res => res.json())
      .catch(err => this.handleError(err));
  }

  //uploadfiles
  uploadFiles(obj, url) {

    const headers = new Headers();
    headers.append('SecureCode', 'S8W8A8M5I1R8O7Y4N8A8NI');
    // headers.append('Content-Type', 'application/json');

    let options1 = new RequestOptions({ headers: headers });


    console.log(obj);
    let formData: FormData = new FormData();
    formData.append('file', obj);

    return this.http.post(this.path + url, formData, options1).map(res => res.json())
      .catch(err => this.handleError(err));
  }


  //post methode.
  PostMethodWithHeaders(obj, url) {
    // let body = JSON.stringify(obj);
    // const headers = new Headers();
    // headers.append('Content-Type', 'application/json');
    // let options = new RequestOptions({ headers: headers });

    return this.http.post(this.path + url, obj, this.options).map(res => res.json())
      .catch(err => this.handleError(err));

    //return this.http.post("http://ez-korier.com/api/BookingService", body, this.options).map(res=>res.json());
  }

  fnchangeDate(string) {


    var splitedArray: any = string.split("-");
    var Reversesplitedarray: any = splitedArray.reverse();
    return Reversesplitedarray[0] + "-" + Reversesplitedarray[1] + "-" + Reversesplitedarray[2];
  }

  private handleError(error) {
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }

  UploadDocuments(obj, url) {

    // const headers = new Headers();
    // let options = new RequestOptions({ headers: headers });


    let AadharformData: FormData = new FormData();
    AadharformData.append('file', obj.AadharDoc);

    let DLformData: FormData = new FormData();
    DLformData.append('file', obj.DLDoc);

    let ProfilePicData: FormData = new FormData();
    ProfilePicData.append('file', obj.ProfilePic);



    return Observable.forkJoin(

      this.http.post(this.path + url, AadharformData, this.options).map(res => res.json()).catch(err => this.handleError(err)),
      this.http.post(this.path + url, DLformData, this.options).map(res => res.json()).catch(err => this.handleError(err)),
      this.http.post(this.path + url, ProfilePicData, this.options).map(res => res.json()).catch(err => this.handleError(err))

    );
  }


  public requestDataFromMultipleSources(objURL: any, objParams: any): Observable<any[]> {
    let response1 = this.getmethod(objURL.urlBookItems);
    let response2 = this.getmethod(objURL.urlRates);
    let response3 = this.getwithId(objURL.urlCancelCharges, objParams.CustomerID);
    console.log("")
    return Observable.forkJoin([response1, response2, response3]);
  }



  // Key press events for character restrictions

  fnNumbers(event) {
    // return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    if (event.charCode < 48 || event.charCode > 57) {
      return false;
    }
  }

  fnNumbersWithDot(event) {
    if (event.charCode != 46 && event.charCode < 48 || event.charCode > 57) {
      return false;
    }
  }

  fnAlphabets(event) {

    if (event.charCode != 32 && event.charCode <= 46 || (event.charCode > 46 && event.charCode < 65) || (event.charCode > 90 && event.charCode < 97) || event.charCode > 122) {
      // Restrict Numbers and dot (.)
      return false;
    }
  }

  fnBlockSpecialchar(event) {
    if (event.charCode != 32 && event.charCode < 48 || (event.charCode > 57 && event.charCode < 65) || (event.charCode > 90 && event.charCode < 97) || event.charCode > 122) {
      event.preventDefault();
    }
  }

  fnValidateEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
  }

  fnGetUniqueNo() {
    let code = "8885187488123456789";
    var text = "";
    for (var i = 0; i < 5; i++)
      text += code.charAt(Math.floor(Math.random() * code.length));

    return text;
  }


}
