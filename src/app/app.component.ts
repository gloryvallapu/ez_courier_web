import { Component } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { CustomServiceService } from '../app/ez-services/custom-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';


  isLoggedIn$: Observable<boolean>;
  httpLoader:any;

  constructor(private Service: CustomServiceService) { }

  ngOnInit() {
    this.isLoggedIn$ = this.Service.isLoggedIn; // {2}
  }


}
